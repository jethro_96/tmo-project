(function($) {
$('input[type="file"]').bind('change', function() {
$("#img_text").html($('input[type="file"]').val());
});
})


$(function() {
  $('i[name="daterange"]').daterangepicker({
    opens: 'center'
  }, function(start, end, label) {
    selectedStartDate = start.format('DD-MM-YYYY');
    selectedEndDate = end.format('DD-MM-YYYY');
    var startDate = document.getElementById('startDate');
    var endDate = document.getElementById('endDate');
    startDate.value = selectedStartDate.toString();
    endDate.value = selectedEndDate.toString();
  });
});

function addGraph(){
document.getElementById('graphremoval').innerHTML = ('<div class="card shadow mb-4"><!-- Card Header - Dropdown --><div class="card-header py-3 d-flex flex-row align-items-center justify-content-between" ><h6 class="m-0 font-weight-bold text-primary"><div id="GraphHeader">Call Catergory Graph 1 Week</div></h6><div class="dropdown no-arrow"><a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i></a><div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" id="changeGraphColumns"></div></div></div><!-- Card Body --><div class="card-body" ><div class="chart-area" id="AreaChart"><canvas id="1weekareachart"></canvas></div></div></div>'
);}
  var columnNumber = 1;
  function CallCatergory(columnNumber){
    addGraph();
    upDateTable(one_month_res.Categorys);
    var areachart = document.getElementById('AreaChart');
    var newScript = document.createElement("script");
    var plot_col_idx = 3;
    var plot_col_name = one_month_res.Categorys[0][plot_col_idx];
    document.getElementById('AreaChart').innerHTML = ("<canvas id='1monthsareachart'></canvas>");
    document.getElementById('changeGraphColumns').innerHTML = ('<div class="dropdown-header">Change Graph Plot</div><div class="dropdown-divider"></div><a class="dropdown-item" id="3months" href="javascript:graphSeleciton(1)">Calls</a><div class="dropdown-divider"></div><a class="dropdown-item" id="6months" href="javascript:graphSeleciton(2)">Transactions</a><div class="dropdown-divider"></div><a class="dropdown-item" id="12months" href="javascript:graphSeleciton(3)">TTV</a><div class="dropdown-divider"></div><a class="dropdown-item" id="12months" href="javascript:graphSeleciton(4)">Converstion Rate</a><div class="dropdown-divider"></div><a class="dropdown-item" id="12months" href="javascript:graphSeleciton(5)">ATV</a>');
    CreateBarChart(one_month_res.Categorys, columnNumber, "1monthsareachart");
    document.getElementById('GraphHeader').innerHTML = ("TTV split by Category");

    areachart.appendChild(newScript);
    RemovePieGraph();
  }
  function PhoneRespones(columnNumber){
    addGraph();
    upDateTable(one_month_res.Response);
    // responsesTables();
    var areachart = document.getElementById('AreaChart');
    var newScript = document.createElement("script");
    document.getElementById('AreaChart').innerHTML = ("<canvas id='1monthsareachart'></canvas>");
    document.getElementById('GraphHeader').innerHTML = ("Phone Responses");
    document.getElementById('changeGraphColumns').innerHTML = ('<div class="dropdown-header">Change Graph Plot</div><div class="dropdown-divider"></div><a class="dropdown-item" id="3months" href="javascript:graphSeleciton(6)">Total</a><div class="dropdown-divider"></div><a class="dropdown-item" id="6months" href="javascript:graphSeleciton(7)">Percentage</a>');
    CreateLineChart([["test", 1], ["20/3", 20], ["21/3", 15], ["22/3", 18]], columnNumber, "1monthsareachart");

    areachart.appendChild(newScript);
    RemovePieGraph();
  }
  function Region(columnNumber){
    addGraph();
    var areachart = document.getElementById('AreaChart');
    var newScript = document.createElement("script");
    // addPieGraph();
    document.getElementById('GraphHeader').innerHTML = ("Call Region");
    document.getElementById('AreaChart').innerHTML = ("<canvas id='1monthsareachart'></canvas>");
    document.getElementById('changeGraphColumns').innerHTML = ('<div class="dropdown-header">Change Graph Plot</div><div class="dropdown-divider"></div><a class="dropdown-item" id="3months" href="javascript:graphSeleciton(1)">Calls</a><div class="dropdown-divider"></div><a class="dropdown-item" id="6months" href="javascript:graphSeleciton(2)">% of Calls</a><div class="dropdown-divider"></div><a class="dropdown-item" id="12months" href="javascript:graphSeleciton(3)">Transactions</a><div class="dropdown-divider"></div><a class="dropdown-item" id="12months"href="javascript:graphSeleciton(4)">% of Transactions</a><div class="dropdown-divider"></div><a class="dropdown-item" id="12months" href="javascript:graphSeleciton(5)">TTV</a><div class="dropdown-divider"></div><a class="dropdown-item" id="12months" href="javascript:graphSeleciton(5)">Converstion Rate</a>');
    CreateBarChart(one_month_res.RegionAnalysis, columnNumber, "1monthsareachart");

    upDateTable(one_month_res.RegionAnalysis);

    areachart.appendChild(newScript);

  }
  function Product(){
    upDateTable(one_month_res.ProductType);
    var areachart = document.getElementById('AreaChart');
    var newScript = document.createElement("script");
    document.getElementById('GraphHeader').innerHTML = ("");
    document.getElementById('AreaChart').innerHTML = ("");
    document.getElementById('graphremoval').innerHTML = ("");

    RemovePieGraph();
  }
  function Caller(){
    addGraph();
    upDateTable(one_month_res.CallerIntent);
    var areachart = document.getElementById('AreaChart');
    var newScript = document.createElement("script");
    document.getElementById('GraphHeader').innerHTML = ("TTV split by Caller Intent");
    document.getElementById('AreaChart').innerHTML = ("<canvas id='1monthsareachart'></canvas>");
    CreateBarChart(one_month_res.CallerIntent, 2, "1monthsareachart");
    document.getElementById('graphremoval').innerHTML = ("");

    areachart.appendChild(newScript);
    RemovePieGraph();
  }
  function Referral(){
    // alert(one_month_res.Referral);
    addGraph();
    upDateTable(one_month_res.Referral);
    var areachart = document.getElementById('AreaChart');
    var newScript = document.createElement("script");
    document.getElementById('GraphHeader').innerHTML = ("TTV by referal page");
    document.getElementById('AreaChart').innerHTML = ("<canvas id='1monthsareachart'></canvas>");
    CreateBarChart(one_month_res.Referral.slice(0, 4), 2, "1monthsareachart");
    document.getElementById('graphremoval').innerHTML = ("");
    RemovePieGraph();
  }

  function addPieGraph(){
    var areachart = document.getElementById('pieGraphinsertion');
    document.getElementById('pieGraphinsertion').innerHTML = ('<div class="col-xl-25 col-lg-25"><div class="card shadow mb-4"><div class="card-header py-3 d-flex flex-row align-items-center justify-content-between"><h6 class="m-0 font-weight-bold text-primary">Transaction Percentage</h6><div class="dropdown no-arrow"><a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i></a><div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink"><div class="dropdown-header">Dropdown Header:</div><a class="dropdown-item" href="#">Change Channel?</a><a class="dropdown-item" href="#">Time Length?</a><div class="dropdown-divider"></div><a class="dropdown-item" href="#">Remove?</a></div></div></div><div class="card-body"><div class="chart-pie pt-4 pb-2"><canvas id="myPieChart"></canvas></div><div class="mt-4 text-center small"><span class="mr-2"></div></div></div></div></div>'
);



    var newScript = document.createElement("script");
    newScript.src = "js/delacon/referral_Pie/referral_PieChart.js";
    areachart.appendChild(newScript);
  }

  function RemovePieGraph(){
    var areachart = document.getElementById('pieGraphinsertion');
    document.getElementById('pieGraphinsertion').innerHTML = ('');
  }

  function SelectCatergory(){
      //find catergory
      var deleconCatergory = document.getElementById('delaconoptions');
      var deleconOption = deleconCatergory.options[deleconCatergory.selectedIndex];
      //find time frame

      //find selected catergory
      function getSelectedOption(deleconCatergory) {

          for ( var i = 0, len = deleconCatergory.options.length; i < len; i++ ) {
              opt = deleconCatergory.options[i];
              if ( opt.selected === true ) {
                  break;
              }
        }
        return opt;
    }

    var selectedOption  = getSelectedOption(delaconoptions);
    // var selectedTimeFrame  = getSelectedTimeFrame(deleconTimeFrame);
    var selectedTimeFrame  = "one month";
    if(selectedOption.value == "catergory"){
      CallCatergory();
    }
    if(selectedOption.value == "phone"){
      PhoneRespones();
    }
    if(selectedOption.value == "referral"){
      Referral();

    }
    if(selectedOption.value == "region"){
      Region();
      addPieGraph();
    }
    if(selectedOption.value == "product"){
      Product();
    }
    if(selectedOption.value == "caller"){
      Caller();
    }
}
function upDateTable(array) {
    var tableString = "";
    tableString += "<thead> <tr>";

    for(var i = 0; i < array[0].length; i++){
      tableString += "<th>";
      tableString += array[0][i];
      tableString += "</th>";
    }
    tableString += "</tr></thead>";
    tableString += "<tbody>";

    for(var i = 1; i < array.length; i++){
      tableString += "<tr>";
      for(var j = 0; j < array[i].length; j ++){
        tableString += "<td>";
        tableString += array[i][j];
        tableString += "</td>"
      }
      tableString += "</tr>";
    }
    tableString += "</tbody>";

    document.getElementById('dataTable').innerHTML = (tableString);
}

function graphSeleciton(columnSelect){
  if(columnSelect == 1){
    CallCatergory(1);
  }
  if(columnSelect == 2){
    CallCatergory(2);
  }
  if(columnSelect == 3){
    CallCatergory(3);
  }
  if(columnSelect == 4){
    CallCatergory(4);
  }
  if(columnSelect == 5){
    CallCatergory(5);
  }
  if(columnSelect == 6){
    PhoneRespones(1);
  }
  if(columnSelect == 7){
    PhoneRespones(2);
  }
  if(columnSelect == 8){
    Region(1);
  }
  if(columnSelect == 9){
    Region(2);
  }
  if(columnSelect == 10){
    Region(3);
  }
  if(columnSelect == 11){
    Region(4);
  }
  if(columnSelect == 12){
    Region(5);
  }
  if(columnSelect == 13){
    Region(6);
  }

}

var pro_type = one_month_res.ProductType;
var ttv_val = pro_type[pro_type.length-1][2] + pro_type[pro_type.length-1][4];
var total_trans = pro_type[pro_type.length-1][1] + pro_type[pro_type.length-1][3];
var atv_val = ttv_val/total_trans;
document.getElementById("displayttv").innerHTML = (one_month_res.overallVals[1][3]);
document.getElementById("displayatv").innerHTML = (one_month_res.overallVals[1][4]);
document.getElementById("displaytotaltrans").innerHTML = (one_month_res.overallVals[1][1]);
document.getElementById("numOfCalls").innerHTML = (one_month_res.overallVals[1][0]);
document.getElementById("covRate").innerHTML = (one_month_res.overallVals[1][2]);
