// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

function number_format(number, decimals, dec_point, thousands_sep) {
  // *     example: number_format(1234.56, 2, ',', ' ');
  // *     return: '1 234,56'
  number = (number + '').replace(',', '').replace(' ', '');
  var n = !isFinite(+number) ? 0 : +number,
    prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
    sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
    dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
    s = '',
    toFixedFix = function(n, prec) {
      var k = Math.pow(10, prec);
      return '' + Math.round(n * k) / k;
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || '').length < prec) {
    s[1] = s[1] || '';
    s[1] += new Array(prec - s[1].length + 1).join('0');
  }
  return s.join(dec);
}

function CreateBarChart(graphVals, columnNum, elementID, suffix) {
    var barNames = [];
    var barVals = [];
    for (var i = 1; i < graphVals.length - 1; i++) {
        // alert(graphVals[i])
        barNames.push(graphVals[i][0]);
        barVals.push(graphVals[i][columnNum]);
    }
    var chart_top = Math.max(...barVals);
    // var ctx = document.getElementById("1monthsareachart");
    var ctx = document.getElementById(elementID);
    var myBarChart = new Chart(ctx, {
      type: 'bar',
      data: {
        // labels: [ 'Welcome Journey', 'Mastercard Daily Journey', 'Upsell Journey' ],
        labels: barNames,
        datasets: [{
          label: "Revenue",
          backgroundColor: "#4e73df",
          hoverBackgroundColor: "#2e59d9",
          borderColor: "#4e73df",
          // data:  graphVals ,
          data:  barVals ,
          // data:  [ 200, 359.65000000000003, 617.845, 1000.61, 1302.43 ] ,
        }],
      },
      options: {
        maintainAspectRatio: false,
        layout: {
          padding: {
            left: 10,
            right: 25,
            top: 25,
            bottom: 0
          }
        },
        scales: {
          xAxes: [{
            time: {
              unit: 'month'
            },
            gridLines: {
              display: false,
              drawBorder: false
            },
            ticks: {
              maxTicksLimit: 6
            },
            maxBarThickness: 25,
          }],
          yAxes: [{
            ticks: {
              min: 0,
              max: chart_top + chart_top * 0.1,
              maxTicksLimit: 5,
              padding: 10,
              // Include a dollar sign in the ticks
              callback: function(value, index, values) {
                  if (suffix == "%") {
                      return number_format(value) + suffix;
                  } else {
                      return suffix + number_format(value);
                  }
              }
            },
            gridLines: {
              color: "rgb(234, 236, 244)",
              zeroLineColor: "rgb(234, 236, 244)",
              drawBorder: false,
              borderDash: [2],
              zeroLineBorderDash: [2]
            }
          }],
        },
        legend: {
          display: false
        },
        tooltips: {
          titleMarginBottom: 10,
          titleFontColor: '#6e707e',
          titleFontSize: 14,
          backgroundColor: "rgb(255,255,255)",
          bodyFontColor: "#858796",
          borderColor: '#dddfeb',
          borderWidth: 1,
          xPadding: 15,
          yPadding: 15,
          displayColors: false,
          caretPadding: 10,
          callbacks: {
            label: function(tooltipItem, chart) {
              var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
              return datasetLabel + ': $' + number_format(tooltipItem.yLabel);
            }
          }
        },
      }
    });
}

function CreateLineChart(graphVals, columnNum, elementID) {
    var lineNames = [];
    var lineVals = [[], [], []];
    for (var i = 1; i < graphVals.length; i++) {
        // alert(graphVals[i])
        lineNames.push(display_date(graphVals[i][0]));
        lineVals[0].push(graphVals[i][1]);
        lineVals[1].push(graphVals[i][2]);
        lineVals[2].push(graphVals[i][3]);
    }
    lineNames = ["Mon 10/Feb", "Tues 11/Feb", "Wed 12/Feb"];
    lineVals = [[50, 30, 45], [2, 3, 7], [8, 5, 1]];
    var chart_top = Math.max(...lineVals);
    var ctx = document.getElementById(elementID);
    // var ctx = document.getElementById("1monthsareachart");
    var myLineChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: lineNames,
        datasets: [{
          label: "Normal",
          lineTension: 0.3,
          backgroundColor: "rgba(78, 115, 223, 0.05)",
          borderColor: "rgba(78, 115, 223, 1)",
          pointRadius: 3,
          pointBackgroundColor: "rgba(78, 115, 223, 1)",
          pointBorderColor: "rgba(78, 115, 223, 1)",
          pointHoverRadius: 3,
          pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
          pointHoverBorderColor: "rgba(78, 115, 223, 1)",
          pointHitRadius: 10,
          pointBorderWidth: 2,
          data: lineVals[0]
          },{
            label: "No Answer",
            lineTension: 0.3,
            backgroundColor: "rgba(220, 64, 64, 0.05)",
            borderColor: "rgba(220, 64, 64, 1)",
            pointRadius: 3,
            pointBackgroundColor: "rgba(220, 64, 64, 1)",
            pointBorderColor: "rgba(220, 64, 64, 1)",
            pointHoverRadius: 3,
            pointHoverBackgroundColor: "rgba(220, 64, 64, 1)",
            pointHoverBorderColor: "rgba(220, 64, 64, 1)",
            pointHitRadius: 10,
            pointBorderWidth: 2,
            data: lineVals[1]
        },{
          label: "Busy",
          lineTension: 0.3,
          backgroundColor: "rgba(228, 228, 88, 0.05)",
          borderColor: "rgba(228, 228, 88, 1)",
          pointRadius: 3,
          pointBackgroundColor: "rgba(228, 228, 88, 1)",
          pointBorderColor: "rgba(228, 228, 88, 1)",
          pointHoverRadius: 3,
          pointHoverBackgroundColor: "rgba(228, 228, 88, 1)",
          pointHoverBorderColor: "rgba(228, 228, 88, 1)",
          pointHitRadius: 10,
          pointBorderWidth: 2,
          data: lineVals[2]
        }],
      },
      options: {
        maintainAspectRatio: false,
        layout: {
          padding: {
            left: 10,
            right: 25,
            top: 25,
            bottom: 0
          }
        },
        scales: {
          xAxes: [{
            time: {
              unit: 'date'
            },
            gridLines: {
              display: false,
              drawBorder: false
            },
            ticks: {
              maxTicksLimit: 7
            }
          }],
          yAxes: [{
            ticks: {
              maxTicksLimit: 5,
              padding: 10,
              // Include a dollar sign in the ticks
              callback: function(value, index, values) {
                return number_format(value);
              }
            },
            gridLines: {
              color: "rgb(234, 236, 244)",
              zeroLineColor: "rgb(234, 236, 244)",
              drawBorder: false,
              borderDash: [2],
              zeroLineBorderDash: [2]
            }
          }],
        },
        legend: {
          display: true
        },
        tooltips: {
          backgroundColor: "rgb(255,255,255)",
          bodyFontColor: "#858796",
          titleMarginBottom: 10,
          titleFontColor: '#6e707e',
          titleFontSize: 14,
          borderColor: '#dddfeb',
          borderWidth: 1,
          xPadding: 15,
          yPadding: 15,
          displayColors: false,
          intersect: false,
          mode: 'index',
          caretPadding: 10,
          callbacks: {
            label: function(tooltipItem, chart) {
              var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
              return datasetLabel + ': ' + number_format(tooltipItem.yLabel);
            }
          }
        }
      }
    });
}

function display_date(date) {
    date = new Date(date);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var month = months[date.getMonth()];
    var days_of_week = ['Mon', 'Tues', "Wed", "Thur", "Fri", "Sat", "Sun"]
    var day_of_week = days_of_week[date.getDay()];
    return day_of_week + " " + date.getDate() + "/" + month;
}

function CreatePieChart(graphVals, columnNum, elementID) {
    var names = [];
    var vals = [];
    for (var i = 1; i < graphVals.length-1; i++) {
        names.push(graphVals[i][0]);
        vals.push(graphVals[i][columnNum]);
    }
    var ctx = document.getElementById(elementID);
    var myPieChart = new Chart(ctx, {
      type: 'doughnut',
      data: {
        labels: names,
        datasets: [{
          data: vals,
          backgroundColor: ['#4e73df', '#1cc88a', '#36b9cc','#058105' ],
          hoverBackgroundColor: ['#2e59d9', '#17a673', '#2c9faf', '#04A204'],
          hoverBorderColor: "rgba(234, 236, 244, 1)",
        }],
      },
      options: {
        maintainAspectRatio: false,
        tooltips: {
          backgroundColor: "rgb(255,255,255)",
          bodyFontColor: "#858796",
          borderColor: '#dddfeb',
          borderWidth: 1,
          xPadding: 15,
          yPadding: 15,
          displayColors: false,
          caretPadding: 10,
        },
        legend: {
          display: false
        },
        cutoutPercentage: 80,
      },
    });
}
