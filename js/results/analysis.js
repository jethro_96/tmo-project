var mongoose = require('mongoose');

// var GTSdata = mongoose.model('GTSTransaction');
// var Delacon = mongoose.model('Delacon');
// var SFMCData = mongoose.model('SFMC');
// var Person = mongoose.model('Person');
// var DelaconAnalysis = mongoose.model('DelaconAnalysis');

function update_duplicates(array) {
    // Function to find multiple calls that are accosicated with one transaction
    // and removing the association of all but one of the calls
    var transaction_list = []
    for (var i = 0; i < array.length; i++) {
        if (array[i].Transactions != null) {
            var appeared = false;
            for (var j = 0; j < transaction_list.length; j++) {
                if (transaction_list[j] == String(array[i].Transactions._id)) {
                    appeared = true;
                }
            }
            if (appeared) {
                array[i].Attributable = false;
                array[i].Transactions = null;
            } else {
                transaction_list.push(String(array[i].Transactions._id));
            }
        }
    }
    return array;
}

module.exports = {
    delacon_outputs: function(data, period) {
        // Function that takes a series of calls with associated transactions
        // and performs analysis on these calls
        data = update_duplicates(data);

        // Prep for Categories
        var category = [["Category", "Calls", "Transactions", "TTV", "Converstion rate", "ATV"]];

        // Prep for areas Analysis
        var areas = [["Region Name", "Calls", "% of calls", "Transactions", "% of Transactions", "TTV", "Converstion rate"]];

        var trafficSources = [["Referral from", "Calls", "Transactions", "TTV"]];

        // Prep for day analysis
        var callsPerDay = [["Call Date", "Answered", "No Answer", "Busy"]];

        // Prep for Response analysis
        var callReponses = [["Response Type", "Total", "Percentage"], ["Normal", 0, 0], ["No answer", 0, 0], ["Busy", 0, 0], ["TOTAL", 0, 0]];

        // Define array for products sold
        var productsSold = [["Product Name", "Buy transactions", "Buy TTV", "Sell transactions", "Sell TTV"]];
        // Define array for CallerIntent
        var callerIntent = [["Caller Intent", "Transactions", "TTV"]];
        var callerIntentTotals = ["TOTAL", 0, 0];

        // Define array for total TTV and ATV
        var transactionVals = [["Total Calls", "Total Attributable Transactions", "Conversion Rate", "TTV", "ATV"], [0, 0, 0, 0, 0]];

        // Iterate through every call
        for (var i = 0; i < data.length; i++) {
            // Most outputs need to check if a variable already exists in the output arrays
            // Reset boolean variables governing if variable already exists
            var alreadyAppeared = {categoryAppeared: false, gmbAppeared: false,
                trafficSourcesAppeared: false, productNameAppeared: false,
                callerIntentAppeared: false}
            transactionVals[1][0] ++;
            if (data[i].Attributable) {
                // Increment products sold info if already appeared
                for (var j = 1; j < productsSold.length; j++) {
                    if (data[i].Transactions.ProductName == productsSold[j][0]) {
                        alreadyAppeared.productNameAppeared = true;
                        if (data[i].Transactions.TradeDirection == "BUY") {
                            productsSold[j][1]++;
                            productsSold[j][2] += data[i].Transactions.DealValue;
                        } else {
                            productsSold[j][3]++;
                            productsSold[j][4] += data[i].Transactions.DealValue;
                        }
                    }
                }
                // Increment caller Intent if already appeared
                if (data[i].DtmfDescription == "") {
                    alreadyAppeared.callerIntentAppeared = true;
                }
                for (var j = 1; j < callerIntent.length; j++) {
                    if (data[i].DtmfDescription == callerIntent[j][0] && !alreadyAppeared.callerIntentAppeared) {
                        alreadyAppeared.callerIntentAppeared = true;
                        callerIntent[j][1]++;
                        callerIntent[j][2] += data[i].Transactions.DealValue;
                        callerIntentTotals[1]++;
                        callerIntentTotals[2] += data[i].Transactions.DealValue;
                    }
                }
                // Add to the total TTV
                transactionVals[1][1] ++;
                transactionVals[1][3] += data[i].Transactions.DealValue;

                // Add another product given it has not yet appeared
                if (!alreadyAppeared.productNameAppeared) {
                    if (data[i].Transactions.TradeDirection == "BUY") {
                        productsSold.push([data[i].Transactions.ProductName, 1, data[i].Transactions.DealValue, 0, 0]);;
                    } else {
                        productsSold.push([data[i].Transactions.ProductName, 0, 0, 1, data[i].Transactions.DealValue]);;
                    }
                }
                // Add another caller intent given it has not yet appeared
                if (!alreadyAppeared.callerIntentAppeared) {
                    callerIntent.push([data[i].DtmfDescription, 1, data[i].Transactions.DealValue]);
                    callerIntentTotals[1]++;
                    callerIntentTotals[2] += data[i].Transactions.DealValue;
                }
            }
            // Category  and Area based analysis
            if (data[i].BusinessCatagory.split(" - ")[1] != null) {
                // Category
                for (var j = 1; j < category.length; j++) {
                    if ("LSM " + data[i].BusinessCatagory.split(" - ")[1] == category[j][0]) {
                        alreadyAppeared.categoryAppeared = true;
                        category[j][1]++;
                        if (data[i].Attributable) {
                            category[j][2]++;
                            category[j][3] += data[i].Transactions.DealValue;
                        }
                    }
                }

                // Area
                for (var j = 1; j < areas.length; j++) {
                    if (data[i].BusinessCatagory.split(" - ")[0] == areas[j][0]) {
                        alreadyAppeared.gmbAppeared = true;
                        areas[j][1]++;
                        // areas[areas.length - 1][1]++;
                        if (data[i].Attributable) {
                            areas[j][3]++;
                            // areas[areas.length - 1][3]++;
                            areas[j][5] += data[i].Transactions.DealValue;
                            // areas[areas.length - 1][5] += data[i].Transactions.DealValue;
                        }
                    }
                }
            } else {
                for (var j = 1; j < category.length; j++) {
                    if (data[i].BusinessCatagory == category[j][0]) {
                        alreadyAppeared.categoryAppeared = true;
                        category[j][1]++;
                        if (data[i].Attributable) {
                            category[j][2]++;
                            category[j][3] += data[i].Transactions.DealValue;
                        }
                    }
                }
            }

            // Source incrementation
            if (data[i].SearchEngine == "") {
                if (data[i].BusinessCatagory.split(" - ")[1] == "Google My Business Profile") {
                    alreadyAppeared.trafficSourcesAppeared = true;
                }
            }
            if (data[i].SearchEngine == "referral" && !alreadyAppeared.trafficSourcesAppeared) {
                for (var j = 1; j < trafficSources.length; j++) {
                    if (data[i].Referral.split(".")[1] == trafficSources[j][0]) {
                        alreadyAppeared.trafficSourcesAppeared = true;
                        trafficSources[j][1]++;
                        if (data[i].Attributable) {
                            trafficSources[j][2]++;
                            trafficSources[j][3] += data[i].Transactions.DealValue;
                        }
                    }
                }
            } else if (!alreadyAppeared.trafficSourcesAppeared) {
                for (var j = 1; j < trafficSources.length; j++) {
                    if (data[i].SearchEngine + " " + data[i].Type == trafficSources[j][0]) {
                        alreadyAppeared.trafficSourcesAppeared = true;
                        trafficSources[j][1]++;
                        if (data[i].Attributable) {
                            trafficSources[j][2]++;
                            trafficSources[j][3] += data[i].Transactions.DealValue;
                        }
                    }
                }
            }

            // Day Analysis
            for (var j = 1; j < callsPerDay.length; j++) {
                var date = new Date(data[i].EventDate);
                date.setHours(0,0,0,0)
                if (date.getTime() == callsPerDay[j][0].getTime()) {
                    alreadyAppeared.productNameAppeared = true;
                    if (data[i].Result == "Normal") {
                        callsPerDay[j][1]++;
                    } else if (data[i].Result == "No answer") {
                        callsPerDay[j][2]++;
                    } else {
                        callsPerDay[j][3]++;
                    }
                }
            }

            // Call response rates
            callReponses[4][1]++;
            if (data[i].Result == "Normal") {
                callReponses[1][1]++;
            } else if (data[i].Result == "No answer") {
                callReponses[2][1]++;
            } else {
                callReponses[3][1]++;
            }

            // Category adding
            if (!alreadyAppeared.categoryAppeared) {
                if (data[i].BusinessCatagory.split(" - ")[1] != null) {
                    if (data[i].Attributable) {
                        category.push(["LSM " + data[i].BusinessCatagory.split(" - ")[1], 1,  1, data[i].Transactions.DealValue, 0, 0]);
                    } else {
                        category.push(["LSM " + data[i].BusinessCatagory.split(" - ")[1], 1,  0, 0, 0, 0]);
                    }
                } else {
                    if (data[i].Attributable) {
                        category.push([data[i].BusinessCatagory, 1,  1, data[i].Transactions.DealValue, 0, 0]);
                    } else {
                        category.push([data[i].BusinessCatagory, 1,  0, 0, 0, 0]);
                    }
                }
            }

            // Area Adding
            if (!alreadyAppeared.gmbAppeared && data[i].BusinessCatagory.split(" - ")[1] != null) {
                if (data[i].Attributable) {
                    areas.push([data[i].BusinessCatagory.split(" - ")[0], 1, 0, 1, 0, data[i].Transactions.DealValue, 0]);
                } else {
                    areas.push([data[i].BusinessCatagory.split(" - ")[0], 1, 0, 0, 0, 0, 0]);
                }
            }

            // Adding to source analysis
            if (!alreadyAppeared.trafficSourcesAppeared) {
                if (data[i].SearchEngine == "referral") {
                    if (data[i].Attributable) {
                        trafficSources.push([data[i].Referral.split(".")[1], 1, 1, data[i].Transactions.DealValue]);
                    } else {
                        trafficSources.push([data[i].Referral.split(".")[1], 1, 0, 0]);
                    }
                } else {
                    if (data[i].Attributable) {
                        trafficSources.push([data[i].SearchEngine + " " + data[i].Type, 1, 1, data[i].Transactions.DealValue]);
                    } else {
                        trafficSources.push([data[i].SearchEngine + " " + data[i].Type, 1, 0, 0]);
                    }
                }
            }

            // Adding to day analysis
            if (!alreadyAppeared.productNameAppeared) {
                var date = new Date(data[i].EventDate);
                date.setHours(0,0,0,0)
                if (data[i].Result == "Normal") {
                    callsPerDay.push([date, 1, 0, 0]);
                } else if (data[i].Result == "No answer") {
                    callsPerDay.push([date, 0, 1, 0]);
                } else {
                    callsPerDay.push([date, 0, 0, 1]);
                }
            }
        }
        // Category Final Touches
        var totals = ["TOTAL", 0, 0, 0, 0];
        for (var i = 1; i < category.length; i++) {
            category[i][3] = Math.round(category[i][3] * 100) / 100
            category[i][4] = Math.round(category[i][2]/category[i][1] * 100 * 100) / 100;
            if (category[i][2] != 0) {
                category[i][5] = Math.round(category[i][3]/category[i][2] * 100) / 100;
            } else {
                category[i][5] = "-";
            }
            totals[1] += category[i][1];
            totals[2] += category[i][2];
            totals[3] += category[i][3];
        }
        totals[3] = Math.round(totals[3] * 100) / 100
        totals[4] = Math.round(totals[2]/totals[1] * 100 * 100) / 100;
        totals[5] = Math.round(totals[3]/totals[2] * 100) / 100;
        category.push(totals);

        //Area Final touches
        areas.push(["TOTAL", 0, 0, 0, 0, 0, 0])
        var areas_total_row = areas.length - 1
        for (var i = 1; i < areas_total_row; i++) {
            areas[areas_total_row][1] += areas[i][1];
            areas[areas_total_row][3] += areas[i][3];
            areas[areas_total_row][5] += areas[i][5];
        }
        for (var i = 1; i <= areas_total_row; i++) {
            areas[i][2] = Math.round(areas[i][1]/areas[areas_total_row][1] * 100 * 100) / 100;
            areas[i][4] = Math.round(areas[i][3]/areas[areas_total_row][3] * 100 * 100) / 100;
            areas[i][5] = Math.round(areas[i][5] * 100) / 100;
            areas[i][6] = Math.round(areas[i][3]/areas[i][1] * 100 * 100) / 100;
        }

        // Source final touches
        for (var i = 1; i < trafficSources.length; i++) {
            trafficSources[i][3] = Math.round(trafficSources[i][3] * 100) / 100;
        }

        // Call response rate final touches
        for (var i = 1; i < callReponses.length; i++) {
            callReponses[i][2] = Math.round(callReponses[i][1]/data.length * 100 * 100) / 100;
        }

        // Normalise the TTV values
        var productsTotal = ["TOTAL", 0, 0, 0, 0]
        for (var i = 1; i < productsSold.length; i++) {
            productsSold[i][2] = Math.round(productsSold[i][2] * 100) / 100;
            productsSold[i][4] = Math.round(productsSold[i][4] * 100) / 100;
            productsTotal[1] += productsSold[i][1];
            productsTotal[2] += productsSold[i][2];
            productsTotal[3] += productsSold[i][3];
            productsTotal[4] += productsSold[i][4];
        }
        productsSold.push(productsTotal);
        callerIntent.push(callerIntentTotals);
        for (var i = 1; i < callerIntent.length; i++) {
            callerIntent[i][2] = Math.round(callerIntent[i][2] * 100) / 100;
        }
        transactionVals[1][2] = (transactionVals[1][1]/transactionVals[1][0] * 100).toFixed(2) + "%";
        transactionVals[1][3] = transactionVals[1][3].toFixed(2);
        transactionVals[1][4] = "$" + (transactionVals[1][3]/transactionVals[1][1]).toFixed(2);
        transactionVals[1][3] = "$" + transactionVals[1][3];

        var outputObject = {AssignmentPeriod: period,
            RegionAnalysis: areas,
            Categorys: category,
            Referral: trafficSources,
            Date: callsPerDay,
            Response: callReponses,
            ProductType: productsSold,
            CallerIntent: callerIntent,
            overallVals: transactionVals
        }
        return outputObject;
    },
    VoucherAnalysis: function() {


    },

    SFMCAnalysis: function(days, startDate, endDate) {
        // Aggregation function which finds emails opened between two dates
        // then finds transactions by people. Removes transactions that are
        // more then x days (As defined by the days variable) after email open.
        // removes all by 1 entry if there is multiple transaction linked with
        // one email or if there is multiple emails linked with one transaction.
        // Then groups by email category and totaling the number of emails,
        // totaling the transaction values and averaging the transaction values
        // for each category
        const sfmcAggregate = [
            // Finds emails between two input dates
            {
                $match: {
                    $expr: {
                        $and: [
                            {$gt: ["$EventDate", startDate]},
                            {$lt: ["$EventDate", endDate]}
                        ]

                    }
                }
            },
            // Links emails to transactions
            {
                $lookup: {
                    from: "gtstransactions",
                    localField: "PID",
                    foreignField: "PID",
                    as: "Transactions"
                }
            },
            {
                $unwind: "$Transactions"
            },
            // Drops values and calculates two new values
            {
                $project: {
                    EmailCategory: 1,
                    EventDate: 1,
                    Transactions: 1,
                    AssignmentCutOffDate: {
                        $subtract: ["$Transactions.DealCreateDate", days*24*60*60*1000]
                    },
                    DateDiff: {
                        $abs: {
                            $subtract: ["$Transactions.DealCreateDate", "$EventDate"]
                        }
                    }
                }
            },
            // Drop emails if email open date time is to far away from transaction
            // or if email was opened after transaction
            {
                $match: {
                    $expr: {
                        $and: [
                            {$gt: ["$EventDate", "$AssignmentCutOffDate"]},
                            {$lt: ["$EventDate", "$Transactions.DealCreateDate"]}
                        ]

                    }
                }
            },
            // Remove multiple emails to one transaction
            {
                $group: {
                    _id: "$Transactions",
                    "doc":{"$first":"$$ROOT"},
                    MinDiff: {$min: "$DateDiff"}
                }
            },
            {
                "$replaceRoot":
                {"newRoot":"$doc"}
            },
            // Remove multiple transactions to one email
            {
                $group: {
                    _id: "$_id",
                    "doc":{"$first":"$$ROOT"},
                    MinDiff: {$min: "$DateDiff"}
                }
            },
            {
                "$replaceRoot":
                {"newRoot":"$doc"}
            },
            // Count for results
            {
                $group: {
                    _id: "$EmailCategory",
                    "Transaction Count": { "$sum": 1 },
                    "TTV": { "$sum": "$Transactions.DealValue" },
                    "ATV": { "$avg": "$Transactions.DealValue" }
                }
            }
        ];
        return sfmcAggregate;
    },

    aggregateDelacon: function(period, startDate, endDate) {
        var pipeline = [
            // Finds calls between two inputted dates
            {
                $match: {
                    $expr: {
                        $and: [
                            {$gt: ["$EventDate", startDate]},
                            {$lt: ["$EventDate", endDate]}
                        ]

                    }
                }
            },
            // Links remaining calls to transactions
            {
                $lookup: {
                    from: "gtstransactions",
                    localField: "PID",
                    foreignField: "PID",
                    as: "Transactions"
                }
            },
            {
                $unwind: {
                    path: "$Transactions",
                    preserveNullAndEmptyArrays: true
                }
            },
            // Calculate values import for later steps
            {
                $project: {
                    Exchange: 1,
                    City: 1,
                    State: 1,
                    EventDate: 1,
                    Result: 1,
                    BusinessName: 1,
                    BusinessCatagory: 1,
                    CallDuration: 1,
                    SearchEngine: 1,
                    Type: 1,
                    Keywords: 1,
                    Referral: 1,
                    LandingPage: 1,
                    CurrentPageReference: 1,
                    AdCopy: 1,
                    AdwordsCampaign: 1,
                    Dtmf: 1,
                    DtmfDescription: 1,
                    Transactions: 1,
                    Attributable: {
                        $lt: ["$EventDate", "$Transactions.DealCreateDate"]
                    },
                    AssignmentCutOffDate: {
                        $subtract: ["$Transactions.DealCreateDate", period*24*60*60*1000]
                    },
                    DateDiff: {
                        $cond: {
                            if: {
                                $lt: ["$EventDate", "$Transactions.DealCreateDate"]
                            },
                            then: {
                                $subtract: ["$Transactions.DealCreateDate", "$EventDate"]
                            },
                            else: period*24*60*60*1000
                        }
                    }
                }
            },
            // Remove calls if time between call and transaction is too large
            {
                $match: {
                    $expr: {
                        $or: [
                            {$gt: ["$EventDate", "$AssignmentCutOffDate"]},
                            {$eq: [null, "$AssignmentCutOffDate"]}
                        ]
                    }
                }
            },
            // Remove multiple transactions to one email
            {
                $group: {
                    _id: "$_id",
                    "doc":{"$first":"$$ROOT"},
                    MinDiff: {$min: "$DateDiff"}
                }
            },
            {
                "$replaceRoot":
                {"newRoot":"$doc"}
            },
            // Sort by time between call and transaction in ascending order
            {
                $sort: {
                    DateDiff: 1
                }
            }
        ];
        return pipeline;
    }
};
