var express = require('express');
var csv = require("fast-csv");
var router = express.Router();
var fs = require('fs');
var app = express();

var mongoose = require('mongoose');

var GTSdata = mongoose.model('GTSTransaction');
var Delacon = mongoose.model('Delacon');
var SFMCData = mongoose.model('SFMC');
var Person = mongoose.model('Person');

const json2csv = require('json2csv').parse;

module.exports = {
    ImportDelaconData: function(fileLocation) {
        // Loads delacon data from csv at specified file location
        var csvfile = __dirname + fileLocation;
        var stream = fs.createReadStream(csvfile);

        var Delacondatapoints = []
        var Delaconcsvstream = csv.parse({ headers: true }).on("data", function(data){
            var callDate = DateTime_From_CSV(data["Date"], data["Time"], false, false);

            // Creates person in person db with phone number found in csv or gets the
            // person ID from person db if phone number has already been used
            var personExistsPromise = new Promise(function (resolve, reject) {
                var callFromNumber = data["Dial From"].substr(2, data["Dial From"].length);
                query = {PhoneNumber: callFromNumber};
                update = {PhoneNumber: callFromNumber};
                Person.findOneAndUpdate(query, update, {upsert: true, new: true}, function(err, personEntry) {
                    if (!err) {
                        console.log(personEntry);
                        resolve(personEntry.id);
                    } else {
                        reject(console.log(err));
                    }
                });
            });
            // Updates the delacon database with information from csv and person ID
            // Does not change anything if call already exisits in db
            personExistsPromise.then(function(result) {
                var item = {
                    Exchange: data["Exchange"],
                    City: data["City"],
                    State: data["State"],
                    EventDate: callDate,
                    Result: data["Result"],
                    BusinessName: data["Business Name"],
                    BusinessCatagory: data["Business Category"],
                    CallDuration: data["Time(sec)"],
                    SearchEngine: data["Search Engine"],
                    Type: data["Type"],
                    Keywords: data["Keywords"],
                    Referral: data["Referral"],
                    LandingPage: data["Landing Page"],
                    CurrentPageReference: data["Current Page Reference"],
                    AdCopy: data["AdCopy"],
                    AdwordsCampaign: data["Adwords Campaign"],
                    Dtmf: data["Dtmf"],
                    DtmfDescription: data["DtmfDescription"],
                    PID: result
                };
                Delacon.findOneAndUpdate(item, item, {upsert: true, new: true}, function(err, doc) {
                    if (err) return console.log(err);
                    return console.log("Data Updated");
                });
            });
        });

        stream.pipe(Delaconcsvstream);
    },

    CreateCSV: function(data, fields, filename) {
        // Function to create csv
        var csv = json2csv(data, {fields});
        fs.writeFile(__dirname + "/../public/files/" + filename + ".csv", csv, function(err) {
            if (err) {console.log(err);}
            console.log('file saved');
        });
    },

    ImportGTSTransaction: function(fileLocation) {
        // Updates the gts database with information from transaction information csv
        // Updates if transaction already exisits in db and creates if not
        var csvfile = __dirname + fileLocation;
        var stream = fs.createReadStream(csvfile);
        mongoose.set('useFindAndModify', false);
        var GTS2datapoints = []
        var GTS2csvstream = csv.parse({ headers: true }).on("data", function(data){
            var query = {'DealCode': data["DealCode"]};
            var updateTo = {'DealValue': Number(data["DealValue"].replace(/,/g, '')), 'TradeDirection': data["TradeDirection"], 'ProductName': data["ProductName"]};
            GTSdata.findOneAndUpdate(query, updateTo, {upsert: true, new: true}, function(err, doc){
                if (err) return console.log(err);
                return console.log("Data Updated");
            });
        });

        stream.pipe(GTS2csvstream);
    },

    ImportGTSCustomer: function(fileLocation) {
        // Loads gts customer data from csv at specified file location
        var csvfile = __dirname + fileLocation;
        var stream = fs.createReadStream(csvfile);

        var GTSdatapoints = []
        var GTS1csvstream = csv.parse({ headers: true }).on("data", function(data){
            var transactionDate = DateTime_From_CSV(data["OccurredOn"], null, false, true);

            // Creates person in person db with phone number found in csv or gets the
            // person ID from person db if phone number has already been used
            var personExistsPromise = new Promise(function (resolve, reject) {
                var query;
                if (data["Email"] == "") {
                    query = {PhoneNumber: data["PhoneNumber"]};
                } else if (data["PhoneNumber"] == "") {
                    query = {Email: data["Email"]};
                } else {
                    query = {$or: [{Email: data["Email"]}, {PhoneNumber: data["PhoneNumber"]}]};
                }

                var update = {PhoneNumber: data["PhoneNumber"], Email: data["Email"]};
                Person.findOneAndUpdate(query, update, {upsert: true, new: true}, function(err, personEntry) {
                    if (!err) {
                        console.log(personEntry);
                        resolve(personEntry.id);
                    } else {
                        reject(console.log(err));
                    }
                });
            });

            // Updates the gts database with information from customer information csv and person id
            // Updates if transaction already exisits in db and creates if not
            personExistsPromise.then(function(result) {
                var item = {DealCode: data["DealCode"],
                    PID: result,
                    DealCreateDate: transactionDate
                };
                var query = {'DealCode': data["DealCode"]};
                GTSdata.findOneAndUpdate(query, item, {upsert: true, new: true}, function(err, doc){
                    if (err) return console.log(err);
                    return console.log("Data Updated");
                });
            });
        });

        stream.pipe(GTS1csvstream);
    },

    ImportSFMCData: function(fileLocation) {
        // Loads sfmc data from csv at specified file location
        var csvfile = __dirname + fileLocation;
        var stream = fs.createReadStream(csvfile);

        var SFMCcsvstream = csv.parse({ headers: true }).on("data", function(data){
            var emailDate = DateTime_From_CSV(data.EventDate, null, true, false);

            // Creates person in person db with cardholderid found in csv or gets the
            // person ID from person db if cardholderid has already been used
            var personExistsPromise = new Promise(function (resolve, reject) {
                Person.findOneAndUpdate({CardHolderId: data.Cardholderid}, {CardHolderId: data.Cardholderid}, {upsert: true, new: true}, function(err, personEntry) {
                    if (!err) {
                        console.log(personEntry);
                        resolve(personEntry.id);
                    } else {
                        reject(console.log(err));
                    }
                });
            });
            // Updates the sfmc database with information from csv and person id
            // Updates if email already exisits in db and creates if not
            personExistsPromise.then(function(result) {
                var item = {
                    EmailAddress: data.EmailAddress,
                    EventDate: emailDate,
                    EmailCategory: data.EmailCategory,
                    PID: result
                };
                SFMCData.findOneAndUpdate(item, item, {upsert: true, new: true}, function(err, doc){
                    if (err) return console.log(err);
                    return console.log("Data Updated");
                });
            });
        });

        stream.pipe(SFMCcsvstream);
    },

    ImportKPOSData: function(fileLocation) {
        // Loads kpos data from csv at specified file location
        var csvfile = __dirname + fileLocation;
        var stream = fs.createReadStream(csvfile);

        var KPOScsvstream = csv.parse({ headers: true }).on("data", function(data){
            var transactionDate = new Date(data.TransactionDate);
            // Creates person in person db with cardholderid found in csv or gets the
            // person ID from person db if cardholderid has already been used
            var personExistsPromise = new Promise(function (resolve, reject) {
                Person.findOneAndUpdate({CardHolderId: data.CardHolderId}, {CardHolderId: data.CardHolderId}, {upsert: true, new: true}, function(err, personEntry) {
                    if (!err) {
                        console.log(personEntry);
                        resolve(personEntry.id);
                    } else {
                        reject(console.log(err));
                    }
                });
            });
            // Updates the gts database with information from kpos csv and person id
            // Updates if transaction already exisits in db and creates if not
            personExistsPromise.then(function(result) {
                var item = {
                    DealCode: data.SCVId,
                    DealCreateDate: transactionDate,
                    DealValue: data.TotalTransactionValue,
                    TradeDirection: data.TransactionType,
                    PID: result
                };
                var query = {DealCode: data.SCVId};
                GTSdata.findOneAndUpdate(query, item, {upsert: true, new: true}, function(err, doc){
                    if (err) return console.log(err);
                    return console.log("Data Updated");
                });
            });
        });

        stream.pipe(KPOScsvstream);
    }
};

function Date_Upload(date_string, daysecond) {
    // Function to split string containing a date
    var date_split;
    if (date_string.split("/")[1] != null) {
        date_split = date_string.split("/");
    } else {
        date_split = date_string.split("-");
    }
    if (daysecond) {
        var temp = date_split[0];
        date_split[0] = date_split[1];
        date_split[1] = temp;
    }
    date_split[1] = Number(date_split[1]) - 1;
    return date_split;
}

function Time_Upload(time_string) {
    // Function to split string containing a time
    var time_12 = time_string.split(" ");
    var time_split = time_12[0].split(":");
    if (time_12[1] == "PM") {
        time_split[0] = Number(time_split[0]) + 12;
    }
    return time_split;
}

function DateTime_From_CSV(date_string, time_string, century, daysecond) {
    // Function to create datetime from a string
    var call_datetime
    if (time_string != null) {
        var date_split = Date_Upload(date_string, daysecond);
        var time_split = Time_Upload(time_string);
        call_datetime = new Date(date_split[2], date_split[1], date_split[0], time_split[0], time_split[1], 10);
    } else {
        datetime_string = date_string.split(" ");
        var date_split = Date_Upload(datetime_string[0], daysecond);
        var time_split = Time_Upload(datetime_string[1] + " " + datetime_string[2]);
        if (century) {
            call_datetime = new Date(Number("20" + date_split[2]), date_split[1], date_split[0], time_split[0], time_split[1], 10);
        } else {
            call_datetime = new Date(date_split[2], date_split[1], date_split[0], time_split[0], time_split[1], 10);
            console.log(date_split[2], date_split[1], date_split[0], time_split[0], time_split[1], call_datetime);
        }
    }
    return call_datetime;
}
