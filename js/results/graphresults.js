var express = require('express');
var csv = require("fast-csv");
var router = express.Router();
var fs = require('fs');
const db = require('../../_helpers/db');
const json2csv = require('json2csv').parse;

var mongoose = require('mongoose');

var csvFunctions = require("./csvFunctions")
var analysis = require("./analysis");
var fullDataSet;

const Delacon = db.Delacon;
const SFMC = db.sfmc;

var period = 1000;

// function delaconFunction(){
//     var startDate = new Date(2018, 10, 6, 10, 33, 30, 0);
//     var endDate = new Date();
//     const delaconAggregate = Delacon.aggregate(analysis.aggregateDelacon(period, startDate, endDate));
//     var dataSet = delaconAggregate.exec().then(function(result) {
//         fullDataSet = analysis.delacon_outputs(result);
//         return fullDataSet;
//     });
//     return dataSet;
// }
//
// module.exports = {
//   bullshit1 : function(){
//     return delaconFunction().then(function(result) {
//         //console.log(result);
//         fullDataSet = result;
//         return fullDataSet;
//     });
//   }
// }
