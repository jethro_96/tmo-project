// var assert = require('assert');
// const config = require('../config.json');
// const mongoose = require('mongoose');
// mongoose.connect(process.env.MONGODB_URI || config.connectionString, { useCreateIndex: true, useNewUrlParser: true });
// var Delacon = require('../users/Delacon.js');
// var GTSData = require('../users/GTSTransaction.js');
// var Person = require('../users/Person.js');
// var SFMCData = require('../users/SFMC.js');
// var csvFunctions = require("../js/results/csvFunctions");
//
// beforeEach((done) => {
//     Delacon.collection.drop();
//     GTSData.collection.drop();
//     done();
//     // mongoose.connection.collections.delacons.drop(() => {
//     //      //this function runs after the drop is completed
//     //     done(); //go ahead everything is done now.
//     // });
// });
//
// describe('Load Delacon Functions', function() {
//     this.timeout(15000);
//     beforeEach((done) => {
//         // Delacon.drop();
//         // console.log("test");
//         // var item = Delacon({DialFrom: 0291, Exchange: "", DtmfDescription: "Test DTMF"})
//         // item.save(function(err, vals) {
//         //     console.log("test");
//         //     done();
//         // });
//         csvFunctions.ImportDelaconData('../../../public/files/UnitTestFiles/call_log_one_line.csv', function(result) {
//             console.log(result);
//             done();
//         });
//     });
//     it('Loading Delacon', (done) => {
//         Delacon.findOne({}).then((call) => {
//             console.log(call);
//             var actualDate = new Date(call.EventDate);
//             var expectedDate = new Date(2019, 01, 28, 22, 43, 10);
//             assert(call.Exchange === "");
//             assert(call.City === "");
//             assert(call.State === "");
//             assert(call.Result === "No answer");
//             assert(call.BusinessName === "TM WA Carousel - GMB");
//             assert(call.BusinessCatagory === "Titanium - Google My Business Profile");
//             assert(call.CallDuration === "12");
//             assert(call.SearchEngine === "");
//             assert(call.Type === "");
//             assert(call.Keywords === "");
//             assert(call.Referral === "");
//             assert(call.LandingPage === "");
//             assert(call.CurrentPageReference === "");
//             assert(call.AdCopy === "");
//             assert(call.AdwordsCampaign === "");
//             assert(call.Dtmf === "");
//             assert(call.DtmfDescription === null);
//             assert(actualDate.getFullYear() === expectedDate.getFullYear());
//             assert(actualDate.getMonth() === expectedDate.getMonth());
//             assert(actualDate.getDate() === expectedDate.getDate());
//             assert(actualDate.getHours() === expectedDate.getHours());
//             assert(actualDate.getMinutes() === expectedDate.getMinutes());
//             assert(typeof call.PID === "string");
//             done();
//         })
//     });
// });
//
// describe('Load Delacon Functions', function() {
//     this.timeout(15000);
//     beforeEach((done) => {
//         csvFunctions.ImportDelaconData('../../../public/files/UnitTestFiles/call_log_one_line.csv', function(result) {
//             done();
//         });
//     });
//     it('Loading Delacon', (done) => {
//         Delacon.findOne({}).then((call) => {
//             var actualDate = new Date(call.EventDate);
//             var expectedDate = new Date(2019, 01, 28, 22, 43, 10);
//             assert(call.Exchange === "");
//             assert(call.City === "");
//             assert(call.State === "");
//             assert(call.Result === "No answer");
//             assert(call.BusinessName === "TM WA Carousel - GMB");
//             assert(call.BusinessCatagory === "Titanium - Google My Business Profile");
//             assert(call.CallDuration === "12");
//             assert(call.SearchEngine === "");
//             assert(call.Type === "");
//             assert(call.Keywords === "");
//             assert(call.Referral === "");
//             assert(call.LandingPage === "");
//             assert(call.CurrentPageReference === "");
//             assert(call.AdCopy === "");
//             assert(call.AdwordsCampaign === "");
//             assert(call.Dtmf === "");
//             assert(call.DtmfDescription === null);
//             assert(actualDate.getFullYear() === expectedDate.getFullYear());
//             assert(actualDate.getMonth() === expectedDate.getMonth());
//             assert(actualDate.getDate() === expectedDate.getDate());
//             assert(actualDate.getHours() === expectedDate.getHours());
//             assert(actualDate.getMinutes() === expectedDate.getMinutes());
//             assert(typeof call.PID === "string");
//             done();
//         })
//     });
// });
