var assert = require('assert');
var analysis = require("../js/results/analysis");
describe('Analysis Testing', function() {
    describe('Singular Call that is linked', function() {
        var dataSet = [{ _id: '5cf70f87175aac9e008fc8b6',
            PhoneNumber: '99123456',
            Exchange: 'BANKSTOWN',
            City: 'SYDNEY',
            State: 'NSW',
            CallDate: '28-02-19',
            CallTime: '22:38',
            EventDate: new Date(2019, 01, 28, 22, 38, 10),
            Result: 'Normal',
            BusinessName: 'Travel+Money+Oz+-+Website',
            BusinessCatagory: 'Website',
            CallDuration: '244',
            SearchEngine: 'google',
            Type: 'free',
            Keywords: 'no_keyword',
            Referral: 'https://www.google.com/',
            LandingPage: 'https://www.travelmoneyoz.com/travel-money-card',
            CurrentPageReference:
             'https://www.travelmoneyoz.com/travel-money-card/key-to-the-world',
            AdCopy: '',
            AdwordsCampaign: '',
            Dtmf: '2111',
            DtmfDescription: 'Currency Pass Activate / Balance Enq / Pin',
            Attributable: 'yes',
            __v: 0,
            Transactions: {
                DealCode: '102100-059496',
                DealCreateDate: new Date(2019, 05, 30, 22, 38, 10),
                Email: 'john@smith.com',
                DealValue: 112.59,
                Occupation: null,
                ProductName: 'Foreign Cash',
                TradeDirection: 'SELL'}
            }];
        var results = analysis.delacon_outputs(dataSet, 1000);
        it('Testing Category Output', function() {
            var expectedResults = [["Category", "Calls", "Transactions", "TTV", "Converstion rate", "ATV"],
                ["Website", 1, 1, 112.59, 100, 112.59], ["TOTAL", 1, 1, 112.59, 100, 112.59]];
            assert.deepEqual(results.Categorys, expectedResults);
        });
        it('Testing Refferal Output', function() {
            var expectedResults = [["Referral from", "Calls", "Transactions", "TTV"],
            ["google free", 1, 1, 112.59]]
            assert.deepEqual(results.Referral, expectedResults);
        });
        it('Testing Call Date Output', function() {
            var expectedResults = [["Call Date", "Answered", "No Answer", "Busy"],
            [new Date(2019, 1, 28, 0, 0, 0), 1, 0, 0]];
            assert.deepEqual(results.Date, expectedResults);
        });
        it('Testing Call Response Output', function() {
            var expectedResults = [["Response Type", "Total", "Percentage"], ["Normal", 1, 100],
                ["No answer", 0, 0], ["Busy", 0, 0], ["TOTAL", 1, 100]];
            assert.deepEqual(results.Response, expectedResults);
        });
        it('Testing Product Type Output', function() {
            var expectedResults = [["Product Name", "Buy transactions", "Buy TTV", "Sell transactions", "Sell TTV"],
                ["Foreign Cash", 0, 0, 1, 112.59], ["TOTAL", 0, 0, 1, 112.59]];
            assert.deepEqual(results.ProductType, expectedResults);
        });
        it('Testing Area Output', function() {
            var expectedResults = [["Region Name", "Calls", "% of calls", "Transactions", "% of Transactions", "TTV", "Converstion rate"],
                ["TOTAL", 0, NaN, 0, NaN, 0, NaN]];;
            assert.deepEqual(results.RegionAnalysis[0], expectedResults[0]);
            assert.deepEqual(results.RegionAnalysis[1][0], expectedResults[1][0]);
            assert.deepEqual(results.RegionAnalysis[1][1], expectedResults[1][1]);
            assert.deepEqual(results.RegionAnalysis[1][3], expectedResults[1][3]);
            assert.deepEqual(results.RegionAnalysis[1][5], expectedResults[1][5]);
            assert(isNaN(results.RegionAnalysis[1][2]));
            assert(isNaN(results.RegionAnalysis[1][4]));
            assert(isNaN(results.RegionAnalysis[1][6]));
        });
        it('Testing Caller Intent Output', function() {
            var expectedResults = [["Caller Intent", "Transactions", "TTV"],
                ["Currency Pass Activate / Balance Enq / Pin", 1, 112.59], ["TOTAL", 1, 112.59]];
            assert.deepEqual(results.CallerIntent, expectedResults);
        });
        it('Testing Overall Values Output', function() {
            var expectedResults = [["Total Calls", "Total Attributable Transactions", "Conversion Rate", "TTV", "ATV"],
                [1, 1, "100.00%", "$112.59", "$112.59"]];
            assert.deepEqual(results.overallVals, expectedResults);
        });
    });
});
