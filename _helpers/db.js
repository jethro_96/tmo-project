const config = require('../config.json');
const mongoose = require('mongoose');
mongoose.connect(process.env.MONGODB_URI || config.connectionString, { useCreateIndex: true, useNewUrlParser: true });
mongoose.Promise = global.Promise;

module.exports = {
    User: require('../users/user.model'),
    SFMCData : require('../users/SFMC'),
    Person : require('../users/Person'),
    marketing :require('../users/MarketingToTransaction'),
    GTSdata : require('../users/GTSTransaction'),
    Delacon : require('../users/Delacon'),
    kpos : require('../users/KPOSTransaction'),
    DelaconAnalysis : require('../users/DelaconResults')
};
