const expressJwt = require('express-jwt');
const config = require('../config.json');
const userService = require('../users/user.service');

function verifyingToken(req, res){
  var token = req.cookies.token;
  jwt.verify(token, config.secret, (err) => {
    if(err) {

      res.sendStatus(403);
    } else {
      res.json({
        message: 'access granded'
      });
    }
  });
}

module.exports = { checkToken : verifyingToken()};
