$(function(){
    console.log("logging1");
    $("#fetchdata").on('click', function(){

        $.get( "/fetchdata", function( data ) {
            var GTSdatapoints = data['data'];
            $("#trdata").html('');
            $("#message").hide();
            var string = '';
            $.each(GTSdatapoints , function(index, GTSele)  {

                string += '<tr><td>'+(index+1)+'</td><td>'+GTSele['_id']+'</td><td>'+GTSele['name']+'</td><td>'+GTSele['FirstName']+'</td><td>'+GTSele['LastName']+'</td><td>'+GTSele['PhoneNumber']+'</td></tr>';
            });

            $("#trdata").html(string);
        });
    });

    $("#importdata").on('click', function(){
        $.get( "/import", function( data ) {
            $("#message").show().html(data['success']);
        });
    });

});
