var express = require('express');
var path = require('path');

var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var session = require('express-session');
var flash = require('connect-flash');

require("./models/Delacon");
require("./models/GTSTransaction");
require("./models/KPOSTransaction");
require("./models/SFMC")
require("./models/Person")

var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/dbproducts', { useNewUrlParser: true });

var index = require('./routes/index');
var tasks = require('./routes/Download');
var download = require('./routes/download');
var port = 8080;

var app = express();

//View Engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

// Set Static Folder
app.use(express.static(path.join(__dirname, 'client')));


app.use(session({ cookie: { maxAge: 60000 },
                  secret: 'woot',
                  resave: false,
                  saveUninitialized: false}));

// Body Parser MW
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(logger('dev'));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'public')));

app.use(flash());


app.use('/', index);
app.use('/api', tasks);
app.use('/download', download);




// error handler



app.listen(port, function(){
    console.log('Server started on port '+port);
});
