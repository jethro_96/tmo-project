'use strict';

var express = require('express');
var http = require('http');
var path = require('path');
var app = express();
app.use(express.static(__dirname));
const cors = require('cors');
const jwt = require('./_helpers/jwt');
const errorHandler = require('./_helpers/error-handler');
var logger = require('morgan');
var bodyParser = require('body-parser');

var session = require('express-session');
var flash = require('connect-flash');
var cookieParser = require('cookie-parser')
app.use(cookieParser())

// Body Parser MW
app.use(bodyParser.urlencoded({extended: true}));
app.use(logger('dev'));
app.use(bodyParser.json({ type: 'application/json'}));
app.use(bodyParser.json());
app.use(flash());

var mongoose = require('mongoose');

var delacon = require('./routes/delacon');
var sfmc = require('./routes/sfmc')
var logout = require('./routes/logout');

var upload = require('./routes/fileupload');
var download = require('./routes/download');
var register = require('./routes/register');
//users connections and error handling

// api routes
app.use('/users', require('./users/users.controller'));

// global error handler
app.use(errorHandler);

//View Engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

app.use('/', delacon);
app.use('/sfmc', sfmc);
app.use('/upload', upload);
app.use('/download', download);
app.use('/logout', logout);

const port = 8080;

const server = app.listen(port, function () {
    console.log('Server listening on port ' + port);
});
