var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var PersonSchema = new Schema({
    PersonID: {Type: String},
    FirstName: {type: String},
    MiddleName: {type: String},
    LastName: {type: String},
    Address: {type: String},
    DOB: {type: String},
    Occupation: {type: String},
    PhoneNumber: {type: Number},
    Email: {type: String}
});

module.exports = mongoose.model('Person', PersonSchema);
