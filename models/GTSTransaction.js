var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var GTSTransactionSchema = new Schema({
    DealCode: {type: String, Required: 'DealCode cannot be left blank'},
    FirstName: {type: String},
    MiddleName: {type: String},
    LastName: {type: String},
    Address: {type: String},
    DOB: {type: String},
    Occupation: {type: String},
    PhoneNumber: {type: String, Required: 'PhoneNumber cannot be left blank'},
    Email: {type: String, Required: 'Email cannot be left blank'},
    Marketing: {type: String},
    // TradingPeriod: {type: String},
    DealCreateDate: {type: Date},
    ProductName: {type: String},
    DealValue: {type: String},
    TradeDirection: {type: String}
});

module.exports = mongoose.model('GTSTransaction', GTSTransactionSchema);
