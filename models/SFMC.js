var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var SFMCSchema = new Schema({
    EmailAddress: {type: String},
    EventDate: {type: Date},
    EmailCategory: {type: String},
    PID: {type: String},
});

module.exports = mongoose.model('SFMC', SFMCSchema);
