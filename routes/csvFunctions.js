var express = require('express');
var csv = require("fast-csv");
var router = express.Router();
var fs = require('fs');
var app = express();

var mongoose = require('mongoose');

var GTSdata = mongoose.model('GTSTransaction');
var Delacon = mongoose.model('Delacon');
var SFMCData = mongoose.model('SFMC')
var Person = mongoose.model('Person')

const json2csv = require('json2csv').parse;

module.exports = {
    ImportDelaconData: function(fileLocation) {
        //"/../public/files/Delacon.csv"
        var csvfile = __dirname + fileLocation;
        var stream = fs.createReadStream(csvfile);

        var Delacondatapoints = []
        var Delaconcsvstream = csv()
            .on("data", function(data){
                var dateSplit = data[6].split("/");
                var timeSplit = data[7].split(":");
                var callDate
                if (data[7].split(" ")[1] == "PM") {
                    callDate = new Date(dateSplit[2], Number(dateSplit[1]) - 1, dateSplit[0], Number(timeSplit[0]) + 12, Number(timeSplit[1]), 10);
                } else {
                    callDate = new Date(dateSplit[2], Number(dateSplit[1]) - 1, dateSplit[0], timeSplit[0], timeSplit[1], 10);
                }
                // console.log(callDate.getMonth());
                var item = new Delacon({DialFrom: data[0],
                    Exchange: data[1],
                    City: data[2],
                    State: data[3],
                    CallDateTime: callDate,
                    Result: data[8],
                    BusinessName: data[10],
                    BusinessCatagory: data[11],
                    CallDuration: data[12],
                    SearchEngine: data[15],
                    Type: data[16],
                    Keywords: data[17],
                    Referral: data[21],
                    LandingPage: data[22],
                    CurrentPageReference: data[23],
                    AdCopy: data[26],
                    AdwordsCampaign: data[27],
                    Dtmf: data[30],
                    DtmfDescription: data[31]
                });
                item.save(function(error){
                    console.log(item);
                    if(error){
                         throw error;
                    }
                });
        });

        stream.pipe(Delaconcsvstream);
    },

    CreateCSV: function(data, fields, filename) {
        //PrePurchase
        var csv = json2csv(data, {fields});
        fs.writeFile(__dirname + "/../public/files/" + filename + ".csv", csv, function(err) {
            if (err) {console.log(err);}
            console.log('file saved');
        });
    },

    DetermineAttributablility: function() {
        Delacon.find({}, function(err, calls) {
            if (!err) {
                for (var i = 0; i < calls.length; i++) {
                    console.log(calls[i].CallDate)
                    var callFromNumber = calls[i].DialFrom;
                    var query = {'PhoneNumber': "123456"};
                    // var query = {'PhoneNumber': { "$regex": callFromNumber.substr(2, callFromNumber.length), "$options": "i" }};
                    GTSdata.find({}, function(err1, transactions) {
                        console.log(callFromNumber.substr(2, callFromNumber.length));
                        console.log(transactions.length)
                        // var closestTransaction;
                        // var minDateTimeDif = 99999999999;
                        // for (var j = 0; j < transactions.length; j++) {
                        //     var transactionDateSplit = transactions[j].DealCreateDate.split("-");
                        //     var transactionDT = Number(transactionDateSplit[2] + "0" + transactionDateSplit[1] + transactionDateSplit[0] + transactions[j].TradingPeriod.replace(/:/g, ''));
                        //     var currentDTDif = callDT - transactionDT;
                        //     if (Math.abs(currentDTDif) <= Math.abs(minDateTimeDif)) {
                        //         closestTransaction = transactions[j];
                        //         minDateTimeDif = currentDTDif
                        //     }
                        // }
                        // if (minDateTimeDif < 0) {
                        //     calls[i].Attributable = "yes";
                        // }
                        // console.log(i)
                        // calls[i].save(function (err) {
                        //     if (err) {
                        //         return console.log(err);
                        //     } else {
                        //         return console.log("Data Attributed");
                        //     }
                        // });
                    });
                }
            }
        });
        // GTSdata.find({}, function(err, transactions) {
        //     if (!err) {
        //         var callPreTransactions = [];
        //         var callPostTransactions = [];
        //         var counter = 0;
        //         for (var i = 0; i < transactions.length; i++) {
        //             if (transactions[i].DealCode != null) {
        //                 counter++;
        //                 var callDateSplit = transactions[i].CallDate.split("-");
        //                 var transactionDateSplit = transactions[i].DealCreateDate.split("-");
        //                 var callDate = Number(callDateSplit[2] + callDateSplit[1] + callDateSplit[0]);
        //                 var transactionDate = Number(transactionDateSplit[2] + "0" + transactionDateSplit[1] + transactionDateSplit[0]);
        //                 var callTime = Number(transactions[i].CallTime.replace(/:/g, ''));
        //                 var transactionTime = Number(transactions[i].TradingPeriod.replace(/:/g, ''))
        //                 if (callDate < transactionDate) {
        //                     console.log(i + ": Attributable");
        //                     callPreTransactions.push(transactions[i]);
        //                 } else if (callDate = transactionDate && callTime <= transactionTime){
        //                     console.log(i + ": Attributable");
        //                     callPreTransactions.push(transactions[i]);
        //                 } else {
        //                     console.log(i + ": Not Attributable");
        //                     callPostTransactions.push(transactions[i]);
        //                 }
        //             }
        //         }
        //         for (var i = 0; i < callPreTransactions.length; i++) {
        //
        //             GTSdata.findOneAndUpdate({'DealCode': callPreTransactions[i].DealCode}, {'Attributable': "yes"}, function(err, doc){
        //                 if (err) return console.log(err);
        //                 return console.log("Attribution Updated");
        //             });
        //         }
        //     }
        // });
    },

    ImportGTS2Data: function(fileLocation) {
        //"/../public/files/GTS2.csv"
        var csvfile = __dirname + fileLocation;
        var stream = fs.createReadStream(csvfile);
        mongoose.set('useFindAndModify', false);
        console.log("test1");
        var GTS2datapoints = []
        var GTS2csvstream = csv()
            .on("data", function(data){
                var query = {'DealCode': data[3]};
                var updateTo = {'DealValue': data[10], 'TradeDirection': data[11], 'Address': data[15], 'DOB': data[16], 'Occupation': data[17], 'ProductName': data[8]};
                GTSdata.findOneAndUpdate(query, updateTo, function(err, doc){
                    if (err) return console.log(err);
                    return console.log("Data Updated");
                });

        }).on("end", function(){
            // DetermineAttributablility();
        });

        stream.pipe(GTS2csvstream);
    },

    ImportGTS1Data: function(fileLocation) {
        //"/../public/files/GTS1.csv"
        var csvfile = __dirname + fileLocation;
        var stream = fs.createReadStream(csvfile);

        var GTSdatapoints = []
        var GTS1csvstream = csv()
            .on("data", function(data){
                var dateTimeSplit = data[6].split(" ");
                var dateSplit = dateTimeSplit[0].split("/");
                var timeSplit = dateTimeSplit[1].split(":");
                var callDate
                if (dateTimeSplit[2] == "PM") {
                    callDate = new Date(dateSplit[2], Number(dateSplit[0]) - 1, dateSplit[1], Number(timeSplit[0]) + 12, Number(timeSplit[1]), 10, 10);
                } else {
                    callDate = new Date(dateSplit[2], Number(dateSplit[0]) - 1, dateSplit[1], Number(timeSplit[0]), Number(timeSplit[1]), 10, 10);
                }
                var query = {'PhoneNumber': { "$regex": data[10].substr(1, data[10].length), "$options": "i" }};
            //     GTSdata.find(query, function (err, calls) {
            //         if (calls.length >= 1) {
            //              var closestIndex = 0;
            //              var minDateTimeDif = 99999999999;
            //
            //              for (var i = 0; i < calls.length; i++) {
            //                  var CallDateSplit = calls[i].CallDate.split("-");
            //                  var CallTimeSplit = calls[i].CallTime.split(":");
            //                  var callDateTime = Number(Number(CallDateSplit[2]).toString() + Number(CallDateSplit[1]).toString() + Number(CallDateSplit[0]).toString() + Number(CallTimeSplit[0]).toString() + Number(CallTimeSplit[1]));
            //                  var transactionDateTime = Number(Number(dateSplit[2]).toString() + Number(dateSplit[0]).toString() + Number(dateSplit[1]).toString() + Number(inputTime.split(":")[0]) + Number(inputTime.split(":")[1]));
            //                  var currentDateTimeDif = Math.abs(transactionDateTime - callDateTime);
            //                  if (currentDateTimeDif < minDateTimeDif) {
            //                      closestIndex = i;
            //                      minDateTimeDif = currentDateTimeDif;
            //                  }
            //
            //                  if (calls[i].DealCreateDate == null && currentDateTimeDif < minDateTimeDif) {
            //                      closestIndex = i;
            //                      minDateTimeDif = currentDateTimeDif;
            //                  }
            //                  if (calls[i].DealCreateDate != null) {
            //                      var prevTransactionsDateSplit = calls[i].DealCreateDate.split("-");
            //                      var prevTransactionsTimeSplit = calls[i].TradingPeriod.split(":");
            //                      var prevTransactionDateTime = Number(Number(prevTransactionsDateSplit[2]).toString() +
            //                         Number(prevTransactionsDateSplit[1]).toString() + Number(prevTransactionsDateSplit[0]).toString() +
            //                         Number(prevTransactionsTimeSplit[0]).toString() + Number(prevTransactionsTimeSplit[0]).toString());
            //                      console.log(prevTransactionDateTime)
            //                      var currentTransactionDateTimeDif = transactionDateTime - prevTransactionDateTime;
            //                      if (currentTransactionDateTimeDif < 0 && currentDateTimeDif < minDateTimeDif) {
            //                          closestIndex = i;
            //                          minDateTimeDif = transactionDateTime;
            //                      }
            //                  }
            //                  // console.log(callDate);
            //              }
            //
            //              calls[closestIndex].DealCode = data[5];
            //              calls[closestIndex].FirstName = data[7];
            //              calls[closestIndex].MiddleName = data[8];
            //              calls[closestIndex].LastName = data[9];
            //              // calls[closestIndex].PhoneNumber = data[10];
            //              calls[closestIndex].Email = data[11];
            //              calls[closestIndex].Marketing = data[12];
            //              calls[closestIndex].TradingPeriod = inputTime;
            //              calls[closestIndex].DealCreateDate = dateRearranged;
            //              calls[closestIndex].save(function (err) {
            //                  if (err) {
            //                      return console.log(err);
            //                  } else {
            //                      return console.log("Delacon Data added");
            //                  }
            //              });
            //     }
            // });
            var item = new GTSdata({DealCode: data[5],
                FirstName: data[7],
                MiddleName: data[8],
                LastName: data[9],
                PhoneNumber: data[10],
                Email: data[11],
                Marketing: data[12],
                // TradingPeriod: dateTimeSplit[1],
                DealCreateDate: callDate
            });
            item.save(function(error){
                console.log(item);
                if(error){
                     throw error;
                }
            });

        });

        stream.pipe(GTS1csvstream);
    },

    ImportSFMCData: function(fileLocation) {
        //"/../public/files/GTS1.csv"
        var csvfile = __dirname + fileLocation;
        var stream = fs.createReadStream(csvfile);

        var SFMCcsvstream = csv({ headers: true })
            .on("data", function(data){
                console.log(data.EventDate);
                var dateTimeSplit = data.EventDate.split(" ");
                var dateSplit = dateTimeSplit[0].split("/");
                var timeSplit = dateTimeSplit[1].split(":");
                var emailDate
                if (dateTimeSplit[2] == "PM") {
                    data.EventDate = new Date(dateSplit[2], Number(dateSplit[1]) - 1, dateSplit[0], Number(timeSplit[0]) + 12, Number(timeSplit[1]), 10, 10);
                } else {
                    data.EventDate = new Date(dateSplit[2], Number(dateSplit[1]) - 1, dateSplit[0], Number(timeSplit[0]), Number(timeSplit[1]), 10, 10);
                }
                // Person.find({}, function(err, calls) {
                // })
                var item = new SFMCData(data);
                item.save(function(error){
                    console.log(item);
                    if(error){
                         throw error;
                    }
                });

        });

        stream.pipe(SFMCcsvstream);
    }
};
