var express = require('express');
var csv = require("fast-csv");
var router = express.Router();
var fs = require('fs');
const jwt = require('jsonwebtoken');
const config = require('../config.json');


var mongoose = require('mongoose');

var GTSdata = mongoose.model('GTSTransaction');
var Delacon = mongoose.model('Delacon');
var DelaconAnalysis = mongoose.model('DelaconAnalysis');
var analysis = require("../js/results/analysis");
/* GET home page. */
router.get('/', function(req, res, next) {
  //grabs the token that has been sent to the client
  var token = req.cookies.token;
  //verifies that the token collection is correct and valid
  jwt.verify(token, config.secret, (err) => {
    //if incorrect redirect back to the login page
    if(err) {
      res.redirect('/users/login');
    } else {
      var startDate = new Date();
      var assignment_period = 30;
      startDate.setDate(startDate.getDate() - assignment_period);
      var endDate = new Date();
      const delaconAggregate = Delacon.aggregate(analysis.aggregateDelacon(assignment_period, startDate, endDate));
      delaconAggregate.exec().then(function(result) {
          fullDataSet = analysis.delacon_outputs(result, assignment_period);
          res.render('delacon.html', {inputArray: fullDataSet});
      });
    }
  });

});

router.post('/', function(req, res, next){

    var startDate = req.body['startDate'];
    var endDate = req.body['endDate'];
    var period = req.body['days'];
    if(!period){
      period = 1;
    }
    console.log(startDate, endDate);
    startDate = new Date(startDate.split("-")[2], Number(startDate.split("-")[1]) - 1, startDate.split("-")[0]);
    endDate = new Date(endDate.split("-")[2], Number(endDate.split("-")[1]) - 1, endDate.split("-")[0]);

    console.log(startDate, endDate);
    const delaconAggregate = Delacon.aggregate(analysis.aggregateDelacon(period, startDate, endDate));
    delaconAggregate.exec().then(function(result) {
        fullDataSet = analysis.delacon_outputs(result, period);
        res.render('delacon.html', {inputArray: fullDataSet});
    });
});
module.exports = router;
