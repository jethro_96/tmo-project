var express = require('express');
var csv = require("fast-csv");
var router = express.Router();
var fs = require('fs');
var analysis = require("../js/results/analysis");
const jwt = require('jsonwebtoken');
const config = require('../config.json');


var mongoose = require('mongoose');

var GTSdata = mongoose.model('GTSTransaction');
var Delacon = mongoose.model('Delacon');
var SFMCData = mongoose.model('SFMC');


/* GET home page. */
router.get('/', function(req, res, next) {

  var token = req.cookies.token;
  jwt.verify(token, config.secret, (err) => {
    if(err) {
      res.redirect('/logout');
    } else {
        var startDate = new Date();
        var assignment_period = 30;
        startDate.setDate(startDate.getDate() - assignment_period);
        var endDate = new Date();
      var sfmcAggregate = SFMCData.aggregate(analysis.SFMCAnalysis(assignment_period, startDate, endDate))
      sfmcAggregate.exec().then(function(result, err) {
          result.unshift(["Category", "Attributable Transactions", "TTV", "ATV"])
          result.push(["TOTAL", 0, 0, 0])
          for (var i = 1; i < result.length - 1; i++) {
              result[i] = Object.values(result[i])
              result[result.length - 1][1] = result[result.length - 1][1] + result[i][1];
              result[result.length - 1][2] = result[result.length - 1][2] + result[i][2];
          }
          result[result.length - 1][3] = result[result.length - 1][2]/result[result.length - 1][1];
          res.render('sfmc.html', {inputArray: result});
      });
    }
  });
});

router.post('/', function(req, res, next){

    var startDate = req.body['startDate'];
    var endDate = req.body['endDate'];
    var period = req.body['days'];
    if(!period){
      period = 1;
    }
    startDate = new Date(startDate.split("-")[2], Number(startDate.split("-")[1]) - 1, startDate.split("-")[0]);
    endDate = new Date(endDate.split("-")[2], Number(endDate.split("-")[1]) - 1, endDate.split("-")[0]);

    const delaconAggregate = Delacon.aggregate(analysis.aggregateDelacon(period, startDate, endDate));
    delaconAggregate.exec().then(function(result) {
        fullDataSet = analysis.delacon_outputs(result, period);
        res.render('sfmc.html', {inputArray: fullDataSet});
    });
});
module.exports = router;
