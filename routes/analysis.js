module.exports = {
    FullDataSetAnalysis: function(data) {
        // Prep for Categories
        var category = [["Category", "Calls", "Transactions", "TTV", "Converstion rate", "ATV"]];

        // Prep for areas Analysis
        var areas = [["Region Name", "Calls", "% of calls", "Transactions", "% of Transactions", "TTV", "Converstion rate"],
        ["Titanium", 0, 0, 0, 0, 0, 0], ["Evolution", 0, 0, 0, 0, 0, 0], ["Saviges", 0, 0, 0, 0, 0, 0],
        ["Patriots", 0, 0, 0, 0, 0, 0], ["Vicmaniacts", 0, 0, 0, 0, 0, 0], ["TOTAL", 0, 0, 0, 0, 0, 0]];
        // var totalCalls = 0;
        // var totalTransactions = 0;

        // Prep for source Analysis
        var trafficSources = [["Referral from", "Calls", "Transactions", "TTV"],
                        ["google", 0, 0, 0], ["mobile search", 0, 0, 0], ["bing", 0, 0, 0],
                        ["yahoo", 0, 0, 0], ["paid", 0, 0, 0], ["Begin referral", "", "", ""], ["End referral", "", "", ""]];

        // Prep for day analysis
        var callsPerDay = [["Call Date", "Answered", "No Answer", "Busy"]];

        // Prep for Response analysis
        var callReponses = [["Response Type", "Total", "Percentage"], ["Normal", 0, 0], ["No answer", 0, 0], ["Busy", 0, 0], ["TOTAL", 0, 0]];

        // Prep for call duration analysis
        var totalCallDur = 0;

        // Prep for Pre/Post analysis
        var prePostTransactions = [["Call Time", "Num of Calls"], ["Pre transactions", 0], ["Post transaction", 0], ["No transaction", 0]];

        for (var i = 0; i < data.length; i++) {
            var alreadyAppeared =  [false, false, false];
            // Category  and Area based analysis
            if (data[i].BusinessCatagory.split(" - ")[1] != null) {
                // Category
                for (var j = 1; j < category.length; j++) {
                    if ("LSM " + data[i].BusinessCatagory.split(" - ")[1] == category[j][0]) {
                        alreadyAppeared[1] = true;
                        category[j][1]++;
                        if (data[i].Attributable != null) {
                            category[j][2]++;
                            category[j][3] += Number(data[i].DealValue.replace(/,/g, ''));
                        }
                    }
                }

                // Area
                for (var j = 1; j < areas.length; j++) {
                    if (data[i].BusinessCatagory.split(" - ")[0] == areas[j][0]) {
                        areas[j][1]++;
                        areas[areas.length - 1][1]++;
                        if (data[i].Attributable != null) {
                            areas[j][3]++;
                            areas[areas.length - 1][3]++;
                            areas[j][5] += Number(data[i].DealValue.replace(/,/g, ''));
                            areas[areas.length - 1][5] += Number(data[i].DealValue.replace(/,/g, ''));
                        }
                    }
                }
            } else {
                for (var j = 1; j < category.length; j++) {
                    if (data[i].BusinessCatagory == category[j][0]) {
                        alreadyAppeared[1] = true;
                        category[j][1]++;
                        if (data[i].Attributable != null) {
                            category[j][2]++;
                            category[j][3] += Number(data[i].DealValue.replace(/,/g, ''));
                        }
                    }
                }
            }

            // Source incrementation
            if (data[i].SearchEngine == "") {
                if (data[i].BusinessCatagory.split(" - ")[1] == "Google My Business Profile") {
                    alreadyAppeared[2] = true;
                }
            }
            if (data[i].SearchEngine == "referral" && !alreadyAppeared[2]) {
                for (var j = 1; j < trafficSources.length; j++) {
                    if (data[i].Referral.split(".")[1] == trafficSources[j][0]) {
                        alreadyAppeared[2] = true;
                        trafficSources[j][1]++;
                        if (data[i].Attributable != null) {
                            trafficSources[j][2]++;
                            trafficSources[j][3] += Number(data[i].DealValue.replace(/,/g, ''));
                        }
                    }
                }
            } else if (!alreadyAppeared[2]) {
                for (var j = 1; j < trafficSources.length; j++) {
                    if (data[i].SearchEngine == trafficSources[j][0]) {
                        alreadyAppeared[2] = true;
                        if (j < 5 && data[i].Type == "paid") {
                            trafficSources[5][1]++;
                            if (data[i].Attributable != null) {
                                trafficSources[5][2]++;
                                trafficSources[5][3] += Number(data[i].DealValue.replace(/,/g, ''));
                            }
                        } else {
                            trafficSources[j][1]++;
                            if (data[i].Attributable != null) {
                                trafficSources[j][2]++;
                                trafficSources[j][3] += Number(data[i].DealValue.replace(/,/g, ''));
                            }
                        }
                    }
                }
            }

            // Day Analysis
            for (var j = 0; j < callsPerDay.length; j++) {
                if (data[i].CallDate == callsPerDay[j][0]) {
                    alreadyAppeared[3] = true;
                    if (data[i].Result == "Normal") {
                        callsPerDay[j][1]++;
                    } else if (data[i].Result == "No answer") {
                        callsPerDay[j][2]++;
                    } else {
                        callsPerDay[j][3]++;
                    }
                }
            }

            // Call response rates
            callReponses[4][1]++;
            if (data[i].Result == "Normal") {
                callReponses[1][1]++;
            } else if (data[i].Result == "No answer") {
                callReponses[2][1]++;
            } else {
                callReponses[3][1]++;
            }

            // Call duration analysis
            var callDurSplit = data[i].CallDuration.split(":");
            for (var j = 0; j < callDurSplit.length; j++) {
                totalCallDur += Number(callDurSplit[j]) * Math.pow(60, callDurSplit.length - 1 - j);
            }

            // Pre/Post analysis
            if (data[i].Attributable != null) {
                prePostTransactions[1][1]++;
            } else if (data[i].DealCode != null){
                prePostTransactions[2][1]++;
            } else {
                prePostTransactions[3][1]++;
            }

            // Category adding
            if (!alreadyAppeared[1]) {
                if (data[i].BusinessCatagory.split(" - ")[1] != null) {
                    if (data[i].Attributable != null) {
                        category.push(["LSM " + data[i].BusinessCatagory.split(" - ")[1], 1,  1, Number(data[i].DealValue.replace(/,/g, '')), 0, 0]);
                    } else {
                        category.push(["LSM " + data[i].BusinessCatagory.split(" - ")[1], 1,  0, 0, 0, 0]);
                    }
                } else {
                    if (data[i].Attributable != null) {
                        category.push([data[i].BusinessCatagory, 1,  1, Number(data[i].DealValue.replace(/,/g, '')), 0, 0]);
                    } else {
                        category.push([data[i].BusinessCatagory, 1,  0, 0, 0, 0]);
                    }
                }
            }

            // Adding to source analysis
            if (!alreadyAppeared[2]) {
                if (data[i].SearchEngine == "referral") {
                    if (data[i].Attributable != null) {
                        trafficSources.splice(7, 0, [data[i].Referral.split(".")[1], 1, 1, Number(data[i].DealValue.replace(/,/g, ''))]);
                    } else {
                        trafficSources.splice(7, 0, [data[i].Referral.split(".")[1], 1, 0, 0]);
                        // trafficSources.push([data[i].Referral.split(".")[1], 1, 0, 0]);
                    }
                } else {
                    if (data[i].Attributable != null) {
                        trafficSources.push([data[i].SearchEngine, 1, 1, Number(data[i].DealValue.replace(/,/g, ''))]);
                    } else {
                        trafficSources.push([data[i].SearchEngine, 1, 0, 0]);
                    }
                }
            }

            // Adding to day analysis
            if (!alreadyAppeared[3]) {
                if (data[i].Result == "Normal") {
                    callsPerDay.push([data[i].CallDate, 1, 0, 0]);
                } else if (data[i].Result == "No answer") {
                    callsPerDay.push([data[i].CallDate, 0, 1, 0]);
                } else {
                    callsPerDay.push([data[i].CallDate, 0, 0, 1]);
                }
            }
        }
        // Category Final Touches
        var totals = ["TOTAL", 0, 0, 0, 0];
        for (var i = 1; i < category.length; i++) {
            category[i][3] = Math.round(category[i][3] * 100) / 100
            category[i][4] = Math.round(category[i][2]/category[i][1] * 100 * 100) / 100;
            if (category[i][2] != 0) {
                category[i][5] = Math.round(category[i][3]/category[i][2] * 100) / 100;
            } else {
                category[i][5] = "-";
            }
            totals[1] += category[i][1];
            totals[2] += category[i][2];
            totals[3] += category[i][3];
        }
        totals[3] = Math.round(totals[3] * 100) / 100
        totals[4] = Math.round(totals[2]/totals[1] * 100 * 100) / 100;
        totals[5] = Math.round(totals[3]/totals[2] * 100) / 100;
        category.push(totals);

        //Area Final touches
        for (var i = 1; i < areas.length; i++) {
            areas[i][2] = Math.round(areas[i][1]/areas[areas.length - 1][1] * 100 * 100) / 100;
            areas[i][4] = Math.round(areas[i][3]/areas[areas.length - 1][3] * 100 * 100) / 100;
            areas[i][5] = Math.round(areas[i][5] * 100) / 100;
            areas[i][6] = Math.round(areas[i][3]/areas[i][1] * 100 * 100) / 100;
        }

        // Source final touches
        for (var i = 1; i < trafficSources.length; i++) {
            trafficSources[i][3] = Math.round(trafficSources[i][3] * 100) / 100;
        }

        // Day final touches
        var totalCallsPerWeekday = [["Day of the week", "Total Answered Calls", "Total Unanswered Calls", "Total Busy Calls"], ["Sunday", 0, 0, 0], ["Monday", 0, 0, 0],
            ["Tuesday", 0, 0, 0], ["Wednesday", 0, 0, 0], ["Thursday", 0, 0, 0],
            ["Friday", 0, 0, 0], ["Saturday", 0, 0, 0]];
        for (var j = 1; j < callsPerDay.length; j++) {
            var CallDateSplit = callsPerDay[j][0].split("-");
            var date = new Date(Number("20" + CallDateSplit[2]), Number(CallDateSplit[1]) - 1, Number(CallDateSplit[0]));
            callsPerDay[j][0] = totalCallsPerWeekday[date.getDay() + 1][0] + " " + callsPerDay[j][0];
            for (var i = 1; i <= 3; i++) {
                totalCallsPerWeekday[date.getDay() + 1][i] += callsPerDay[j][i];
            }
        }

        // Call response rate final touches
        for (var i = 1; i < callReponses.length; i++) {
            callReponses[i][2] = Math.round(callReponses[i][1]/data.length * 100 * 100) / 100;
        }

        // Call duration final touches
        var callDurationInfo = [["Call Duration", "Days", "Hours", "Minutes", "Seconds"], ["Total call time", 0, 0, 0, 0], ["Average time per call", 0, 0, 0, 0]];
        if (totalCallDur > 60) {
            callDurationInfo[1][3] = Math.floor(totalCallDur/60);
            callDurationInfo[1][4] = totalCallDur%60;
            if (callDurationInfo[1][3] > 60) {
                callDurationInfo[1][2] = Math.floor(callDurationInfo[1][3]/60);
                callDurationInfo[1][3] = callDurationInfo[1][3]%60;
                if (callDurationInfo[1][2] > 24) {
                    callDurationInfo[1][1] = Math.floor(callDurationInfo[1][2]/24);
                    callDurationInfo[1][2] = callDurationInfo[1][2]%24;
                }
            }
        }
        var averageCallDur = totalCallDur/data.length;
        if (averageCallDur > 60) {
            callDurationInfo[2][3] = Math.floor(averageCallDur/60);
            callDurationInfo[2][4] = Math.floor(averageCallDur%60);
            if (callDurationInfo[2][3] > 60) {
                callDurationInfo[2][2] = Math.floor(callDurationInfo[2][3]/60);
                callDurationInfo[2][3] = Math.floor(callDurationInfo[2][3]%60);
                if (callDurationInfo[2][2] > 24) {
                    callDurationInfo[2][1] = Math.floor(callDurationInfo[2][2]/24);
                    callDurationInfo[2][2] = callDurationInfo[2][2]%24;
                }
            }
        }

        return [areas, category, trafficSources, totalCallsPerWeekday, callsPerDay, callReponses, callDurationInfo, prePostTransactions];
    },

    ProductAnalysis: function(data) {
        // Define array for products sold
        var ProductsSold = [["Product Name", "Buy", "Buy Value", "Sold", "Sold Value"]];
        // Define array for CallerIntent
        var callerIntent = [["Caller Intent", "Transactions", "TTV"]];
        var callerIntentTotals = ["TOTAL", 0, 0];

        // Define array for total TTV and ATV
        var transactionVals = [["TTV", "ATV"], [0, 0]];

        for (var i = 0; i < data.length; i++) {
            // Define if value has already appeared
            var alreadyAppeared =  [false, false];
            // Increment products sold info if already appeared
            for (var j = 1; j < ProductsSold.length; j++) {
                if (data[i].ProductName == ProductsSold[j][0]) {
                    alreadyAppeared[0] = true;
                    if (data[i].TradeDirection == "BUY") {
                        ProductsSold[j][1]++;
                        ProductsSold[j][2] += Number(data[i].DealValue.replace(/,/g, ''));
                    } else {
                        ProductsSold[j][3]++;
                        ProductsSold[j][4] += Number(data[i].DealValue.replace(/,/g, ''));
                    }
                }
            }
            // Increment caller Intent if already appeared
            if (data[i].DtmfDescription == "") {
                alreadyAppeared[1] = true;
            }
            for (var j = 1; j < callerIntent.length; j++) {
                if (data[i].DtmfDescription == callerIntent[j][0] && !alreadyAppeared[1]) {
                    alreadyAppeared[1] = true;
                    callerIntent[j][1]++;
                    callerIntent[j][2] += Number(data[i].DealValue.replace(/,/g, ''));
                    callerIntentTotals[1]++;
                    callerIntentTotals[2] += Number(data[i].DealValue.replace(/,/g, ''));
                }
            }
            // Add to the total TTV
            transactionVals[1][0] += Number(data[i].DealValue.replace(/,/g, ''));

            // Add another product given it has not yet appeared
            if (!alreadyAppeared[0]) {
                if (data[i].TradeDirection == "BUY") {
                    ProductsSold.push([data[i].ProductName, 1, Number(data[i].DealValue.replace(/,/g, '')), 0, 0]);;
                } else {
                    ProductsSold.push([data[i].ProductName, 0, 0, 1, Number(data[i].DealValue.replace(/,/g, ''))]);;
                }
            }
            // Add another caller intent given it has not yet appeared
            if (!alreadyAppeared[1]) {
                callerIntent.push([data[i].DtmfDescription, 1, Number(data[i].DealValue.replace(/,/g, ''))]);
            }
        }

        // Normalise the TTV values
        var productsTotal = ["TOTAL", 0, 0, 0, 0]
        for (var i = 1; i < ProductsSold.length; i++) {
            ProductsSold[i][2] = Math.round(ProductsSold[i][2] * 100) / 100;
            ProductsSold[i][4] = Math.round(ProductsSold[i][4] * 100) / 100;
            productsTotal[1] += ProductsSold[i][1];
            productsTotal[2] += ProductsSold[i][2];
            productsTotal[3] += ProductsSold[i][3];
            productsTotal[4] += ProductsSold[i][4];
        }
        ProductsSold.push(productsTotal);
        callerIntent.push(callerIntentTotals);
        for (var i = 1; i < callerIntent.length; i++) {
            callerIntent[i][2] = Math.round(callerIntent[i][2] * 100) / 100;
        }
        transactionVals[1][0] = Math.round(transactionVals[1][0] * 100) / 100;
        transactionVals[1][1] = Math.round((transactionVals[1][0]/data.length) * 100) / 100;
        return [ProductsSold, callerIntent, transactionVals];

    }
};
