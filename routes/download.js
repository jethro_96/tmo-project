var express = require('express');
var csv = require("fast-csv");
var router = express.Router();
var fs = require('fs');

var mongoose = require('mongoose');

var GTSdata = mongoose.model('GTSTransaction');
var Delacon = mongoose.model('Delacon');

const json2csv = require('json2csv').parse;

var csvFunctions = require("./csvFunctions")
var analysis = require("./analysis");

/* GET home page. */
router.get('/', function(req, res, next) {

    res.download(__dirname + '/../public/files/LinkedData.csv');

});
module.exports = router;
