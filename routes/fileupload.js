var express = require('express');
var csv = require("fast-csv");
var router = express.Router();
var fs = require('fs');
const fileUpload = require('express-fileupload');
router.use(fileUpload());

var mongoose = require('mongoose');

var GTSdata = mongoose.model('GTSTransaction');
var Delacon = mongoose.model('Delacon');

const json2csv = require('json2csv').parse;

var csvFunctions = require("../js/results/csvFunctions");

/* GET home page. */
router.post('/', function(req, res){

  console.log(req.files);

  if(Object.keys(req.files).length == 0){
    return res.status(400).send('No File was uploaded.');
  }

  if (req.files.delaconFile != null) {
      req.files.delaconFile.mv(__dirname + '/../public/files/delaconFile.csv', function(err) {
        if (err)
          return res.status(500).send(err);
      });
      csvFunctions.ImportDelaconData('../../../public/files/delaconFile.csv');
  }
  if (req.files.GTSCustomerFile != null) {
      req.files.GTSCustomerFile.mv(__dirname + '/../public/files/GTSCustomerFile.csv', function(err) {
        if (err)
          return res.status(500).send(err);
      });
      csvFunctions.ImportGTSCustomer('../../../public/files/GTSCustomerFile.csv');
  }
  if (req.files.GTSTransactionFile != null) {
      req.files.GTSTransactionFile.mv(__dirname + '/../public/files/GTSTransactionFile.csv', function(err) {
        if (err)
          return res.status(500).send(err);
      });
      csvFunctions.ImportGTSTransaction('../../../public/files/GTSTransactionFile.csv');
  }
  if (req.files.emailFile != null) {
      req.files.emailFile.mv(__dirname + '/../public/files/emailFile.csv', function(err) {
        if (err)
          return res.status(500).send(err);
      });
      csvFunctions.ImportSFMCData('../../../public/files/emailFile.csv');
  }
  if (req.files.KPOSTransactionFile != null) {
      req.files.KPOSTransactionFile.mv(__dirname + '/../public/files/KPOSTransactionFile.csv', function(err) {
        if (err)
          return res.status(500).send(err);
      });
      csvFunctions.ImportKPOSData('../../../public/files/KPOSTransactionFile.csv');
  }

  res.redirect('/');

});

router.get('/', function(req, res, next) {

  res.redirect('/');

});
module.exports = router;
