var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var MarketingToTransactionSchema = new Schema({
    Delacon: {type: Number},
    SFMC: {type: Number},
    Search: {type: Number},
    DealCode: {type: String, ref: 'GTSTransaction'}
});

module.exports = mongoose.model('MtT', MarketingToTransactionSchema);
