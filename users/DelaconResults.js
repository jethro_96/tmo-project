var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var DelaconAnalysisSchema = new Schema({
    AssignmentPeriod: {type: String},
    RegionAnalysis: {type: Array},
    Categorys: {type: Array},
    Referral: {type: Array},
    Weekday: {type: Array},
    Date: {type: Array},
    Response: {type: Array},
    CallDur: {type: Array},
    CallTime: {type: Array},
    ProductType: {type: Array},
    CallerIntent: {type: Array}
});

module.exports = mongoose.model('DelaconAnalysis', DelaconAnalysisSchema);
