var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var GTSTransactionSchema = new Schema({
    DealCode: {type: String},
    DealCreateDate: {type: Date},
    ProductName: {type: String},
    DealValue: {type: Number},
    TradeDirection: {type: String},
    PID: {type: String}
});

module.exports = mongoose.model('GTSTransaction', GTSTransactionSchema);
