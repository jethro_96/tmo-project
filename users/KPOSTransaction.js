var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var KPOSTransactionSchema = new Schema({
    ReceiptUniqueId: {type: String, Required: 'DealCode cannot be left blank'},
    TransactionId: {type: String},
    TransactionPortalId: {type: String},
    TransactionDate: {type: Date},
    T3: {type: String},
    T4: {type: String},
    Brand: {type: String},
    ProductType: {type: String},
    SCVId: {type: String},
    ForeignCurrencyDetails: [{
        CurrencyCode: {type: String},
        CurrencyBaseRate: Number,
        CurrencyFinalRate: Number,
        CurrencyValue: Number,
        AUDCurrencyValue: Number
    }],
    TotalSaleValue: Number,
    VoucherCode: {type: String},
    DiscountValue: Number,
    MerchantFeeValue: Number,
    ReloadFeeValue: Number,
    TotalTransactionValue: Number,
    TransactionType: {type: String},
    CardHolderId: {type: String},
    SalesChannel: {type: String},
    PID: {type: String}
});

module.exports = mongoose.model('KPOSTransaction', KPOSTransactionSchema);
