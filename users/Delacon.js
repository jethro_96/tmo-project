var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var DelaconSchema = new Schema({
    DialFrom: {type: String},
    Exchange: {type: String},
    City: {type: String},
    State: {type: String},
    EventDate: {type: Date},
    Result: {type: String},
    BusinessName: {type: String},
    BusinessCatagory: {type: String},
    CallDuration: {type: String},
    SearchEngine: {type: String},
    Type: {type: String},
    Keywords: {type: String},
    Referral: {type: String},
    LandingPage: {type: String},
    CurrentPageReference: {type: String},
    AdCopy: {type: String},
    AdwordsCampaign: {type: String},
    Dtmf: {type: String},
    DtmfDescription: {type: String},
    PID: {type: String}
});

module.exports = mongoose.model('Delacon', DelaconSchema);
