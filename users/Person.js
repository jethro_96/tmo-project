var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var PersonSchema = new Schema({
    PersonID: {Type: String},
    PhoneNumber: {type: Number},
    Email: {type: String},
    CardHolderId: {type: String}
});

module.exports = mongoose.model('Person', PersonSchema);
